import {
  Component,
  OnInit,
  DoCheck,
  ViewEncapsulation,
  ChangeDetectorRef
} from '@angular/core';

import {
  DomSanitizer
} from '@angular/platform-browser';
import {
  MatIconRegistry,
  MatSnackBar
} from '@angular/material';

import {
  GlobalService
} from '../../services/global/global';
import {
  NavService
} from '../../services/global/nav.service';
import {
  AuthService
} from 'src/app/services/global/auth.service';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import {
  LedgerService
} from 'src/app/services/ledger.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavComponent implements DoCheck {

  countdata;
  count;
  constructor(private router: Router,
    private globalService: GlobalService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private authService: AuthService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private Ledger: LedgerService,
    public navService: NavService,
 
  ) {

    iconRegistry.addSvgIcon(
      'e-suvidha',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/e-suvidhaLogo.svg'));

    iconRegistry.addSvgIcon(
      'notification',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/notification.svg'));

    iconRegistry.addSvgIcon(
      'support',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/support.svg'));

    iconRegistry.addSvgIcon(
      'profile',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/profile.svg'));
  
      const input={
        center_code:sessionStorage.getItem("centre_code")
      }
      this.Ledger.getAppointmentCount(input).then(async (data) => {
       
      
        this.countdata = await data.getAppointmentCount.count;
       
    
      });
      console.log("this is countdata in constructor",this.countdata);
      this.count=this.countdata;
      console.log("count value in ngoninit",this.count);
    }

  ngDoCheck() {

//  if (  setTimeout(() => {
     
//     }, 10000)){
     

// if (this.count == this.countdata) {
//   this.counterdata();
}
  

  ngOnInit() {
    const input={
      center_code:sessionStorage.getItem("centre_code")
    }
    this.Ledger.getAppointmentCount(input).then(async (data) => {
     
    
      this.countdata = await data.getAppointmentCount.count;
      console.log("count",this.countdata);
  
    });
  }
 

  // ngOnChanges() {
  //   // every time the object changes 
  //   // store the new `count`
   
  // }
  username() {
    return sessionStorage.getItem('FullName');
  }

  logout() {
    this.globalService.loaderOn();
    this.authService.logout().then((data) => {
        console.log('----- logout data: ---- ', data);
        if (data.status === 'S') {
          sessionStorage.clear();
          this.router.navigate(['/signin']);
          this.snackBar.open('You have been logged-out.', 'Dismiss', {
            duration: 8000,
          });
        }
      })
      .catch((error) => {
        console.log('----- logout err: ---- ', error);
        this.globalService.loaderOff();
        this.router.navigate(['/mods/dashboard']);
        this.snackBar.open('Logging-out failed. Retry in a few.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });
  }

  changePass() {
    this.router.navigate(['changepassword']);
  }
 viewappointment(){
   this.router.navigate(['viewAppointment']);
 }
}
