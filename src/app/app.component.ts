import { Component } from '@angular/core';
import { GlobalService } from './services/global/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'otc';
  constructor(public globalService: GlobalService) {
  }
}
