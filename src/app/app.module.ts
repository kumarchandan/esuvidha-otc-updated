import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Pipe, PipeTransform } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';
import { NavService } from './services/global/nav.service';
import { AuthModule } from './auth/auth.module';
import { OtcModule } from './otc/otc.module';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/global/auth.service';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { AuthGuard } from './services/global/auth-guard.service';

import { GlobalService } from './services/global/global';
import { MatSnackBarConfig, MatSnackBarModule } from '@angular/material';
import { LedgerService } from './services/ledger.service';
import { CPGaurd } from './services/global/cp-gaurd.service';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AppRoutingModule,
    OtcModule,
    AuthModule,
    GraphQLModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxUiLoaderModule,
    NgIdleKeepaliveModule.forRoot(),
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
  ],
  providers: [
    NavService,
    AuthService,
    AuthGuard,
    CPGaurd,
    GlobalService,
    LedgerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
