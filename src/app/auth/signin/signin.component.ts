import {
  Component,
  OnInit,
  HostListener,
  ViewEncapsulation,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  DomSanitizer
} from '@angular/platform-browser';
import {
  MatIconRegistry,
  MatSnackBar
} from '@angular/material';
import {
  AuthService,
} from '../../services/global/auth.service';
import {
  GlobalService
} from '../../services/global/global';
import {
  Router
} from '@angular/router';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})

export class SigninComponent implements OnInit {

  signinForm: FormGroup;
  counterForm: FormGroup;

  submitted = false;
  submit = false;
  centre_hide = false;
  login_hide = true;

  login_stat = {};
  counters = [];

  constructor(private snackBar: MatSnackBar, private router: Router,private http: HttpClient, private globalService: GlobalService, private authService: AuthService,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    // SVG ICONS
    iconRegistry.addSvgIcon(
      'e-suvidha',
      sanitizer.bypassSecurityTrustResourceUrl('/logo.svg'));

    // Logout
    if (sessionStorage.getItem('counter_no')) {
      console.log(sessionStorage.getItem('counter_no'))
      console.log('Loging out');
      this.authService.logout().then((data) => {
          if (data.status === 'S') {
            this.snackBar.open('You have been logged-out.', 'Dismiss', {
              duration: 8000,
            });
            console.log('is it called?');
            // Remove token from local storage
            sessionStorage.clear();
            globalService.loaderOff();
          }
        })
        .catch((error) => {
          console.log('Logout Error', error);
          this.globalService.loaderOff();
          // this.router.navigate(['/mods/dashboard']);
          this.snackBar.open('Logging-out failed. Retry in a few.', 'Dismiss', {
            duration: 8000,
            horizontalPosition: 'center',
            verticalPosition: 'top'
          });
        });
    } else {
      globalService.loaderOff();
    }
  }

  hide = true;

  ngOnInit() {
    this.centre_hide = false;
    this.login_hide = true;

    this.signinForm = new FormGroup({
      'username': new FormControl(null, [
        Validators.required,
        Validators.pattern('[a-zA-Z0-9]*')
      ]),
      'password': new FormControl(null, Validators.required)
    });

    this.counterForm = new FormGroup({
      'select_counter': new FormControl(null, Validators.required)
    });
  }

  async login() {
    this.submitted = true;
    if (this.signinForm.invalid) {
      return;
    }

    this.globalService.loaderOn();

    this.authService.authenticate(this.signinForm.controls.username.value,
        this.signinForm.controls.password.value).then((data) => {
        this.login_stat = sessionStorage.getItem('login_stat');

        if (this.login_stat === 'Y') {
          this.globalService.loaderOff();
          this.counters = JSON.parse(sessionStorage.getItem('counters'));
          this.centre_hide = true;
          this.login_hide = false;
        } else {
          this.centre_hide = false;
          this.login_hide = true;
        }
        const header = new HttpHeaders({
          'Content-Type': 'application/json'
        });
        this.http.post('https://ticket.esuvidhamitra.com/api/v1/login', {
          "username": sessionStorage.getItem("username").toLowerCase(),
          "password": "esuvidha"
        }, {
          headers: header
        }).subscribe(
          (val => {
            console.log("-------- login response ------------");
            console.log(val["accessToken"]);
            console.log("-------- login response ------------");
            sessionStorage.setItem("access_token", val["accessToken"]);
            this.getUser();
            //global.loaderOff();
          }),
          (err) => {
            console.log(err);
          }
        )
      })
      .catch((err) => {
        this.globalService.loaderOff();
        console.log('caledd ', err);
        if (err.code === 404) {
          return;
        }
        this.snackBar.open('Login Failed! Try Again.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });
  }

  async getUser(){
    const header = new HttpHeaders({
      'Content-Type': 'application/json',
      'accessToken':sessionStorage.getItem("access_token")
    });
    this.http.get(`https://ticket.esuvidhamitra.com/api/v1/users/${sessionStorage.getItem("username").toLowerCase()}`,
   
    {
     headers: header
    }).subscribe(
      (val => {
        console.log('========== val 2 ================')
        console.log(val);
        console.log('=========== val 2 ===============',val["groups"][0])
         //sessionStorage.setItem("access_token", val["accessToken"]);
         sessionStorage.setItem("groups", val["groups"][0]);
         sessionStorage.setItem("users", val["user"]["_id"]);
        //global.loaderOff();
        
      }), (err) => {
console.log(err);
      }
      )
  }
  assignCounter() {
    this.globalService.loaderOn();
    this.submit = true;
    const send_counter = {
      agent_uid: sessionStorage.getItem('agent_uid'),
      centre_code: sessionStorage.getItem('centre_code'),
      counter_no: this.counterForm.controls.select_counter.value
    };

    this.authService.assignCounter(send_counter).then((data) => {
      if (data) {
        this.router.navigate(['/mods/dashboard']);
      } else {
        this.router.navigate(['/signin']);
      }
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.signinForm.controls;
  }

  get g() {
    return this.counterForm.controls;
  }

}
