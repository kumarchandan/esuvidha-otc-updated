import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AuthGuard } from '../services/global/auth-guard.service';
import { AuthComponent } from './auth.component';
import { OtcComponent } from '../otc/otc.component';
import { DashboardComponent } from '../otc/dashboard/dashboard.component';
import { FreezedLedgerComponent } from '../otc/ledger/freezed-ledger/freezed-ledger.component';
import { OpennedLedgerComponent } from '../otc/ledger/openned-ledger/openned-ledger.component';
import { DisputeComponent } from '../otc/ledger/openned-ledger/dispute/dispute.component';
import { DisputeReceiptComponent } from '../otc/ledger/openned-ledger/dispute/dispute-receipt/dispute-receipt.component';
import { ViewTransactionComponent } from '../otc/transaction/view-transaction/view-transaction.component';
import { NewTransactionComponent } from '../otc/transaction/new-transaction/new-transaction.component';
import { RaiseDusputeComponent } from '../otc/dispute/raise-duspute/raise-duspute.component';
import { CheckoutComponent } from '../otc/transaction/new-transaction/checkout/checkout.component';
import { ReceiptComponent } from '../otc/transaction/new-transaction/checkout/receipt/receipt.component';
import { DenominationsComponent } from '../otc/denominations/denominations.component';
import { DenominationsReceiptComponent } from '../otc/denominations/denominations-receipt/denominations-receipt.component';
import { LedgerSummaryComponent } from '../otc/ledger/ledger-summary/ledger-summary.component';
import { CreateTicketComponent } from '../otc/ticket/create-ticket/create-ticket.component';
import { ViewTicketComponent } from '../otc/ticket/view-ticket/view-ticket.component';
import { ViewAppointmentComponent } from '../otc/appointment/view-appointment/view-appointment.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'changepassword',
        component: ChangePasswordComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'forgotpassword',
        component: ForgotPasswordComponent
      },
      {
        path: 'signin',
        component: SigninComponent
      },
      {
        path: 'login',
        redirectTo: '/signin',
        pathMatch: 'full'
      },
      {
        path: '',
        redirectTo: '/signin',
        pathMatch: 'full'
      },
      {
        path: '**',
        redirectTo: '/signin',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: 'mods',
    component: OtcComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'ledger',
        children: [
          {
            path: 'fledger',
            component: FreezedLedgerComponent
          },
          {
            path: 'summary',
            component: LedgerSummaryComponent
          },
          {
            path: 'oledger',
            children: [
              {
                path: '',
                component: OpennedLedgerComponent,
                pathMatch: 'full'
              },
              {
                path: 'dispute',
                children: [
                  {
                    path: 'dispute-receipt',
                    component: DisputeReceiptComponent
                  }
                ]
              }
            ]
          },
        ]
      },
      {
        path: 'vtransaction',
        component: ViewTransactionComponent
      },
      {
        path: 'newtransaction',
        children: [
          {
            path: '',
            component: NewTransactionComponent,
            pathMatch: 'full',
          },
       
        ]
      },
      {
        path: 'any-dispute',
        component: RaiseDusputeComponent,
      },
      {
        path: 'settle-denomination',
        children: [
          {
            path: '',
            component: DenominationsComponent,
            pathMatch: 'full'
          },
          {
            path: 'denominations-receipt',
            component: DenominationsReceiptComponent,
          }
        ]
      },
      {
        path: 'ticket',
        children: [
          {
            path: 'CreateTicket',
            component: CreateTicketComponent
          },
          {
            path: 'ViewTicket',
            component: ViewTicketComponent
          },
         
        ]
      },
      {
        path: 'Appointments',
        children: [
          {
            path: 'viewAppointment',
            component: ViewAppointmentComponent
          },
        
         
        ]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AuthRoutingModule { }
