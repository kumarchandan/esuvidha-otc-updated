import {
  Component,
  OnInit
} from '@angular/core';
import {
  MatIconRegistry,
  MatSnackBar
} from '@angular/material';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  DomSanitizer
} from '@angular/platform-browser';
import {
  GlobalService
} from 'src/app/services/global/global';
import {
  AuthService
} from 'src/app/services/global/auth.service';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changePassForm: FormGroup;
  submitted = false;

  constructor(public router: Router, public authServices: AuthService, public snackBar: MatSnackBar,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public global: GlobalService) {
    iconRegistry.addSvgIcon(
      'e-suvidha',
      sanitizer.bypassSecurityTrustResourceUrl('/logo.svg'));

    this.global.loaderOff();
  }

  ngOnInit() {
    this.changePassForm = new FormGroup({
      'newpassword': new FormControl(null, Validators.required),
      'repeatnewpassword': new FormControl(null, Validators.required),
      'oldpassword': new FormControl(null, Validators.required),
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.changePassForm.controls;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

  submit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.changePassForm.invalid) {
      return;
    }

    this.global.loaderOn();

    this.authServices.changePassword(this.changePassForm.controls.newpassword.value,
        this.changePassForm.controls.repeatnewpassword.value, this.changePassForm.controls.oldpassword.value)
      .then((data) => {
        console.log('gesfssd');
        if (data) {
          this.global.loaderOff();
          if (data.status) {
            this.openSnackBar('Invalid Old Password.', 'Dismiss');
          } else {
            this.openSnackBar('Your password has been changed.', 'Dismiss');
            this.router.navigate(['/']);
          }
        } else {
          this.openSnackBar('Changin password failed. Please try again.', 'Dismiss');
        }
      });
  }

}
