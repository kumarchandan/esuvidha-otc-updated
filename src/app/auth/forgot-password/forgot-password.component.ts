import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  MatIconRegistry,
  MatSnackBar
} from '@angular/material';
import {
  DomSanitizer
} from '@angular/platform-browser';
import {
  GlobalService
} from 'src/app/services/global/global';
import {
  AuthService
} from 'src/app/services/global/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})

export class ForgotPasswordComponent implements OnInit {

  forgetPassForm: FormGroup;
  submitted = false;

  constructor(private snackBar: MatSnackBar, public global: GlobalService,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private authServices: AuthService) {
    global.loaderOff();
    iconRegistry.addSvgIcon(
      'e-suvidha',
      sanitizer.bypassSecurityTrustResourceUrl('/logo.svg'));
  }

  ngOnInit() {
    this.forgetPassForm = new FormGroup({
      'username': new FormControl(null, [
        Validators.required,
        Validators.pattern('[a-zA-Z0-9]*')
      ])
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.forgetPassForm.controls;
  }

  submit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.forgetPassForm.invalid) {
      return;
    }

    this.global.loaderOn();

    this.authServices.forgotPassword(this.forgetPassForm.controls.username.value.toUpperCase()).then((data) => {
      this.global.loaderOff();
      if (data) {
        this.openSnackBar('Please check your email for futher instructions.', '');
      } else {
        this.openSnackBar('Operation failed. Please try again or call your manager.', '');
      }
    })
    .catch((err) => {
      this.global.loaderOff();
      this.openSnackBar('Invalid Username. Please try again.', '');
    });

  }
}
