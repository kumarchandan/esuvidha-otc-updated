import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  NavService
} from 'src/app/services/global/nav.service';
import {
  Idle,
  DEFAULT_INTERRUPTSOURCES
} from '@ng-idle/core';
import {
  NavItem
} from '../components/nav/nav-items';
import {
  AuthService
} from '../services/global/auth.service';
import {
  HAMMER_LOADER, VERSION
} from '@angular/platform-browser';
import {
  GlobalService
} from '../services/global/global';
import {
  Router
} from '@angular/router';
import {
  MatSnackBar
} from '@angular/material';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthComponent implements OnInit {
  @ViewChild('appDrawer', {
    read: '',
    static: true
  }) appDrawer: ElementRef;
  version = VERSION;
  navItems: NavItem[] = [{
      displayName: 'Dashboard',
      iconName: 'dashboard',
      route: 'mods/dashboard'
    },
    {
      displayName: 'Ledger',
      iconName: 'account_balance_wallet',
      route: 'mods/ledger',
      children: [{
          displayName: 'Opened Ledger',
          iconName: 'add_box',
          route: 'mods/ledger/oledger',
        },
        {
          displayName: 'Freezed Ledgers',
          iconName: 'archive',
          route: 'mods/ledger/fledger',
        }
      ]
    },
    {
      displayName: 'View Transaction',
      iconName: 'calendar_view_day',
      route: 'mods/vtransaction',
    },
    {
      displayName: 'Raise Dispute',
      iconName: 'autorenew',
      route: 'mods/any-dispute',
    },
    {
      displayName: 'New Transaction',
      iconName: 'add_box',
      route: 'mods/newtransaction',
      divider: true
    },
    {
      displayName: 'Ticket',
      iconName: 'add_box',
      route: 'mods/newtransaction',
      divider: true,
      children: [{
          displayName: 'Create Ticket',
          iconName: 'add_box',
          route: 'mods/ticket/CreateTicket',
        },
        {
          displayName: 'View Ticket',
          iconName: 'archive',
          route: 'mods/ticket/ViewTicket',
        }
      ]
    },
    {
      displayName: 'Appointments',
      iconName: 'add_box',
      route: 'mods/newtransaction',
      divider: true,
      children: [{
          displayName: 'View Appointment',
          iconName: 'archive',
       
        },

      ]
    }
  ];


  ngOnInit() {}

}
