import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OtcComponent } from './otc.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FreezedLedgerComponent } from './ledger/freezed-ledger/freezed-ledger.component';
import { OpennedLedgerComponent } from './ledger/openned-ledger/openned-ledger.component';
import { DisputeComponent } from './ledger/openned-ledger/dispute/dispute.component';
import { DisputeReceiptComponent } from './ledger/openned-ledger/dispute/dispute-receipt/dispute-receipt.component';
import { ViewTransactionComponent } from './transaction/view-transaction/view-transaction.component';
import { NewTransactionComponent } from './transaction/new-transaction/new-transaction.component';
import { RaiseDusputeComponent } from './dispute/raise-duspute/raise-duspute.component';
import { CheckoutComponent } from './transaction/new-transaction/checkout/checkout.component';
import { ReceiptComponent } from './transaction/new-transaction/checkout/receipt/receipt.component';
import { DenominationsComponent } from './denominations/denominations.component';
import { DenominationsReceiptComponent } from './denominations/denominations-receipt/denominations-receipt.component';
import { AuthGuard } from '../services/global/auth-guard.service';
import { CPGaurd } from '../services/global/cp-gaurd.service';
import { LedgerSummaryComponent } from './ledger/ledger-summary/ledger-summary.component';
import { CreateTicketComponent } from './ticket/create-ticket/create-ticket.component';
import { ViewTicketComponent } from './ticket/view-ticket/view-ticket.component';
import { ViewAppointmentComponent } from './appointment/view-appointment/view-appointment.component';


const routes: Routes = [
  {
    path: 'mods',
    component: OtcComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'ledger',
        children: [
          {
            path: 'fledger',
            component: FreezedLedgerComponent
          },
          {
            path: 'summary',
            component: LedgerSummaryComponent
          },
          {
            path: 'oledger',
            children: [
              {
                path: '',
                component: OpennedLedgerComponent,
                pathMatch: 'full'
              },
              {
                path: 'dispute',
                children: [
                  {
                    path: 'dispute-receipt',
                    component: DisputeReceiptComponent
                  }
                ]
              }
            ]
          },
        ]
      },
      {
        path: 'vtransaction',
        component: ViewTransactionComponent
      },
      {
        path: 'newtransaction',
        children: [
          {
            path: '',
            component: NewTransactionComponent,
            pathMatch: 'full',
          },
          {
            path: 'checkout',
            canActivate: [CPGaurd],
            children: [
              {
                path: '',
                component: CheckoutComponent,
                pathMatch: 'full'
              },
              {
                path: 'receipt',
                component: ReceiptComponent,
              }
            ]
          }
        ]
      },
      {
        path: 'any-dispute',
        component: RaiseDusputeComponent,
      },
      {
        path: 'settle-denomination',
        children: [
          {
            path: '',
            component: DenominationsComponent,
            pathMatch: 'full'
          },
          {
            path: 'denominations-receipt',
            component: DenominationsReceiptComponent,
          }
        ]
      },
      {
        path: 'ticket',
        children: [
          {
            path: 'CreateTicket',
            component: CreateTicketComponent
          },
          {
            path: 'ViewTicket',
            component: ViewTicketComponent
          },
         
        ]
      },
      {
        path: 'Appointments',
        children: [
          {
            path: 'viewAppointment',
            component: ViewAppointmentComponent
          },
        
         
        ]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class OtcRoutingModule { }
