export interface DialogData {
    ledger_uid: string;
    tnx_uid: string;
    bill_amt: string;
    assigned_to: string;
    assigned_to_uid: string;
    status_code: string;
    txn_status_name: string;
    consumer_acct: string;
    consumer_ph: string;
    paid_amt: string;
    tnx_date: string;
    payment_mode: string;
    created_on: string;
    received_amount: string;
    paid_amount: string
    //return_amount: string;
    card_type: string;
    pos_num: string;
    cheque_num: string;
    bank_name: string;
    cheq_due_date: string;
    mode_type: string;
    rtgs_num: string;
    neft_num: string;
}