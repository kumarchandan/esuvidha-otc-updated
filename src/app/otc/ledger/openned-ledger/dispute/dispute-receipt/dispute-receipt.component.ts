import { Component, OnInit, Inject, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import { Router, NavigationEnd } from '@angular/router';

import { LedgerService, ReceiptData } from 'src/app/services/ledger.service';
import { GlobalService } from 'src/app/services/global/global';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dispute-receipt',
  templateUrl: './dispute-receipt.component.html',
  styleUrls: ['./dispute-receipt.component.scss']
})
export class DisputeReceiptComponent implements OnInit, OnDestroy {

  current_date = new Date();
  data;

  constructor( private ledgerService: LedgerService, 
    private router: Router,
    private global: GlobalService, ) { 
    
    this.data = this.ledgerService.sharedData;
    this.ledgerService.getDisputeReceipt().subscribe((data) => {
      console.log(data);

      this.data = data;
      console.log(this.data);
      global.loaderOff();
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onPrint(){
    window.print();
  }

  back(){
    this.router.navigate(['/mods/ledger/oledger']);
  }

}
