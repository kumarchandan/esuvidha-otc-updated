import { Component, OnInit, Inject, ChangeDetectorRef, OnDestroy } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import { LedgerService } from 'src/app/services/ledger.service';
import { GlobalService } from 'src/app/services/global/global';

import { DialogData } from '../dispute-data';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-dispute',
  templateUrl: './dispute.component.html',
  styleUrls: ['./dispute.component.scss']
})
export class DisputeComponent implements OnInit, OnDestroy {
  raiseDispute: FormGroup;

  submitted = false;

  _return = " ";

  constructor( public dialogRef: MatDialogRef<DisputeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, 
    private cdref: ChangeDetectorRef, 
    private ledgerService: LedgerService,
    private router: Router,
    private global: GlobalService, ) { 

      this.raiseDispute = new FormGroup({
        'remarks': new FormControl(null, Validators.required),
        // 'return_mode': new FormControl(null, Validators.required),
        // 'POS': new FormControl("", null),
        // 'cheque_no': new FormControl("", null)
      });

      global.loaderOff();
  }

  ngOnInit() {
    this.raiseDispute = new FormGroup({
      'remarks': new FormControl(null, Validators.required),
      // 'return_mode': new FormControl(null, Validators.required),
      // 'POS': new FormControl("", null),
      // 'cheque_no': new FormControl("", null)
    });

    //this.raiseDispute.controls.return_mode.setValue(this.data.payment_mode);
    this.onChanges();
  }

  onNoClick(): void {
    var return_data = {
      status : 'NO'
    }

    this.dialogRef.close(return_data);
    this.global.loaderOn();
  }

  onChanges(): void {
    // this.raiseDispute.get('return_mode').valueChanges.subscribe(val => {
    //   this._return = val;
    //   console.log(val);
    //   if(this._return == 'CHEQUE'){
    //     this.raiseDispute.controls['cheque_no'].disable();
    //   }
    // });
  }

  onClick(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.raiseDispute.invalid) {
      return;
    }

    this.global.loaderOn();

    var return_data = {
      remarks : this.raiseDispute.controls.remarks.value,
      // amt_return_mode : this.raiseDispute.controls.return_mode.value,
      // pos_id : this.raiseDispute.controls.POS.value,
      // cheque_no : this.raiseDispute.controls.cheque_no.value,
      status : 'YES'
    }

    console.log(return_data);

    var current_date = new Date().toLocaleString().split(", ");
    var time_string = current_date[1].split(":");
    var date_string = current_date[0].split("/");

    var counter_no = sessionStorage.getItem("counter_no");
    
    var receipt_data = {
      //amt_return_pos_id : return_data.pos_id,
      payment_mode : this.data.payment_mode,
      agent_uid : this.data.assigned_to_uid,
      agent_name : this.data.assigned_to,
      agent_username : sessionStorage.getItem("username"),
      //amt_return_mode : return_data.amt_return_mode,
      centre_code : sessionStorage.getItem("centre_code"),
      centre_name : sessionStorage.getItem("centre_name"),
      centre_address : sessionStorage.getItem("centre_address"),
      counter_name : sessionStorage.getItem("counter_no"),
      counter_no : counter_no.slice(counter_no.length - 2, counter_no.length),
      dispute_date : new Date(),
      acct_id : this.data.consumer_acct,
      con_ph_no : this.data.consumer_ph,
      tnx_uid : this.data.tnx_uid,
      paid_amt : this.data.paid_amt,
      bill_amt : this.data.bill_amt,
      ledger_uid : this.data.ledger_uid,
      dispute_id : date_string[2] + date_string[1] + date_string[0] + time_string[0] + time_string[1] + time_string[2],
      received_amount: this.data.received_amount,
      paid_amount: this.data.paid_amount,
      //return_amount: this.data.return_amount,
      card_type: this.data.card_type,
      pos_num: this.data.pos_num,
      cheque_num: this.data.cheque_num,
      bank_name: this.data.bank_name,
      cheq_due_date: this.data.cheq_due_date,
      mode_type: this.data.mode_type,
      rtgs_num: this.data.rtgs_num,
      neft_num: this.data.neft_num,
    }
    console.log(receipt_data)

    this.ledgerService.saveDisputeReceipt(receipt_data);
    
    this.dialogRef.close(return_data);
    //this.router.navigate(['/mods/ledger/oledger/dispute/dispute-receipt']);

  }

  get f() { return this.raiseDispute.controls; }

  ngOnDestroy() {
  }

}
