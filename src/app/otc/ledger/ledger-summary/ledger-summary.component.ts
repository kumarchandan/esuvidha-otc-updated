import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationEnd
} from '@angular/router';
import {
  MatSnackBar,
  MatDialog,
  MatIconRegistry
} from '@angular/material';
import {
  LedgerService
} from 'src/app/services/ledger.service';
import {
  DomSanitizer
} from '@angular/platform-browser';
import { GlobalService } from 'src/app/services/global/global';


var converter = require('number-to-words');

// let TransactionsData: TransactionElement[] = [];

// export interface TransactionElement {
//   tnx_uid: string;
//   service_name: string;
//   createdOn: string;
//   contactNumber: number;
//   status: string;
//   billAmount: number;
//   billType: string;
// }

@Component({
  selector: 'app-ledger-summary',
  templateUrl: './ledger-summary.component.html',
  styleUrls: ['./ledger-summary.component.scss']
})
export class LedgerSummaryComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  public tnx_data;
  public view_tnx = false;
  public counter_no;
  public freezedLD;
  public TransactionsData;
  public ledgerCreatedDate;
  public ledgerUID: string;
  public led_details: any;
  public amtInWords;

  closedLD_id = JSON.parse(sessionStorage.getItem("oledger_uid"));

  constructor(private router: Router,
    private ledgerService: LedgerService,
    public dialog: MatDialog,
    public global: GlobalService,
    private snackBar: MatSnackBar,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
     
    // Svg Init
    iconRegistry.addSvgIcon(
      'profilepic',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/profilepic.svg'));

    iconRegistry.addSvgIcon(
      'ledger-stats',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/ledger-stats.svg'));

    iconRegistry.addSvgIcon(
      'rupee-indian',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/rupee-indian.svg'));

    iconRegistry.addSvgIcon(
      'print',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/print.svg'));

    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/edit.svg'));

    iconRegistry.addSvgIcon(
      'Success',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/Success.svg'));

    iconRegistry.addSvgIcon(
      'table-null',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/table-null.svg'));

    iconRegistry.addSvgIcon(
      'dispute',
      sanitizer.bypassSecurityTrustResourceUrl('../../assets/img/dispute.svg'));

    this.global.loaderOff();

    try {
        const extra = this.router.getCurrentNavigation().extras.state;
        this.ledgerUID = extra.ledger_uid;
        this.ledgerService.callLedgerDetailsQuery(this.ledgerUID).then((data) => {
        console.log('sadasda', data);
        if (data.status = 'F') {
          this.TransactionsData = data.dat;
          this.TransactionsData.forEach(item=>{
            this.ledgerCreatedDate = item.inserted_date;
          })
        }else{
          this.snackBar.open('No Transaction has been done in this ledger', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
        }
        
        this.ledgerService.fetchClosedLedgerQuery(this.ledgerUID, 'FREEZED')
        .then((led_de) => {
          this.led_details = led_de.dat;
          this.led_details.counter_no = sessionStorage.getItem('counter_no');
          this.led_details.name = sessionStorage.getItem('FullName');
          this.led_details.centre_code = sessionStorage.getItem('centre_code');
          this.led_details.created_on = extra.created_on;
          console.log('**************',this.led_details.created_on)
        });
      });
      
    this.ledgerService.fetchFreezedLedgerDenos(this.ledgerUID, 'FREEZED').then((data) => {
      this.global.loaderOff();
      console.log("-------- freezed denos : in deno-slip ---------- ", data.dat);
      if(data.status == 'F'){
        this.freezedLD = data.dat;
      }
      else{
        this.snackBar.open('Freezed Ledger denominations found empty. Either, ledger might not have been freezed or, Try Again, later.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      }
    })
    .catch((error) => {
      this.global.loaderOff();
      this.snackBar.open('Error. Retry.', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    });
    } catch {
      router.navigate(['/mods/ledger/fledger']);
    }
  }

  

  // tslint:disable:max-line-length
  // displayedColumns: string[] = ['tnx_uid', 'service_name', 'created_on', 'contactNumber', 'billAmount', 'receivedAmount', 'status', 'details'];
  // dataSource: MatTableDataSource < TransactionElement > ;

  // @ViewChild(MatPaginator, {
  //   static: true
  // }) paginator: MatPaginator;
  // @ViewChild(MatSort, {
  //   static: true
  // }) sort: MatSort;
  // snackBar: any;
  // ----------------____Denomination SLip--------------
  step = 0;

  ngOnInit() {
  }

  view(ele) {
    this.global.loaderOn();
    console.log('--------- in view ----------- ', ele);
    this.ledgerService.fetchcompleteTnxDtls(ele.tnx_uid).then((data) => {
        console.log('------------ selected tnx data ---------------', data.dat);
        this.tnx_data = data.dat;
        this.tnx_data.in_words = `${converter.toWords(ele.paid_amt)} only`;
        this.tnx_data.amtInWords =  this.tnx_data.in_words.charAt(0).toUpperCase() +  this.tnx_data.in_words.slice(1);
        console.log('==============',this.tnx_data.amtInWords)
        this.tnx_data.username = sessionStorage.getItem('username');

        const cnt = data.dat.counter_no;
        this.counter_no = cnt.slice(cnt.length - 2, cnt.length);
        this.view_tnx = true;
        this.global.loaderOff();
      })
      .catch((err) => {
        this.global.loaderOff();
        this.snackBar.open('Error! Try again.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });
  }

  getAddress() {
    let obj = {
      addressLine: '',
      city: '',
      
    };
    let address = JSON.parse(this.tnx_data.address);
    console.log('------------------------------ ', address);
    obj.addressLine = `${address.AddressLine1} ${'-'} ${address.PinCode}` ;
    console.log('------------------------------------', obj);
    return obj.addressLine;
  }

  getReceiptDate() {
    const receiptDate = new Date();
    console.log(receiptDate);
    const r = receiptDate.getDate() + '/' + (receiptDate.getMonth() + 1) + '/' + receiptDate.getFullYear();
    return r;
  }

  getReceiptTime() {
    const receiptTime = new Date()
    console.log('------------',receiptTime)
    return receiptTime.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
  }

  // applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  onPrint() {
    window.print();
  }

  back() {
    this.router.navigate(['/mods/ledger/fledger']);
  }

  backSingle() {
    this.view_tnx = false;
  }
}
