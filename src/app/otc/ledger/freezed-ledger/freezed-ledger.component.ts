import {
  Component,
  OnInit
} from '@angular/core';
import {
  MatSnackBar,
  MatIconRegistry
} from '@angular/material';
import {
  LedgerService
} from 'src/app/services/ledger.service';
import {
  Router,
  NavigationEnd
} from '@angular/router';
import {
  DomSanitizer
} from '@angular/platform-browser';
import {
  GlobalService
} from 'src/app/services/global/global';
import {
  getLocaleNumberFormat
} from '@angular/common';

@Component({
  selector: 'app-freezed-ledger',
  templateUrl: './freezed-ledger.component.html',
  styleUrls: ['./freezed-ledger.component.scss']
})

export class FreezedLedgerComponent implements OnInit {

  dtOptions: DataTables.Settings = {};

  public TransactionsData = [];
  // public ELEMENT_DATA: LedgerElement[] = [];

  // displayedColumns: string[] = ['ledger_uid', 'created_on', 'cash', 'cheque', 'card', 'status', 'details'];

  // dataSource = new MatTableDataSource < LedgerElement > ();

  // // @ViewChild('table') table: MatTable<Element>;
  // @ViewChild(MatPaginator, {
  //   static: true
  // }) paginator: MatPaginator;
  // @ViewChild(MatSort, {
  //   static: true
  // }) sort: MatSort;

  constructor(private router: Router,
    private ledgerService: LedgerService,
    private snackBar: MatSnackBar,
    private globals: GlobalService,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {

    iconRegistry.addSvgIcon(
      'profilepic',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/img/profilepic.svg'));

    iconRegistry.addSvgIcon(
      'OPEN',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/img/ongoing.svg'));

    iconRegistry.addSvgIcon(
      'CLOSED',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/img/pending.svg'));

    iconRegistry.addSvgIcon(
      'FREEZED',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/img/freezed.svg'));

    iconRegistry.addSvgIcon(
      'rupee-indian',
      sanitizer.bypassSecurityTrustResourceUrl('/assets/img/rupee-indian.svg'));
    this.globals.loaderOn();

  
      this.ledgerService.callLedgerQuery().then((data: Array < Object > ) => {
          globals.loaderOff();
            console.log('****freezed ledger******',data)
            this.TransactionsData = [];
            console.log(data, 'data called?');
            data.forEach((element: any) => {
           // Fetch Date
          // tslint:disable:radix
          const date = new Date(parseInt(element.inserted_date));

          const obj: any = {
            ledger_uid: element.ledger_uid,
            created_on: '' + date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear(),
            total_transaction: element.total_tnx,
            cash_cnt:element.total_cash_cnt,
            cash_amt: element.received_amt_cash,
            cheq_cnt:element.total_cheq_cnt,
            cheque_amt: element.received_amt_chq,
            card_cnt:element.total_card_cnt,
            card_amt: element.received_amt_card,
            dispute_cnt:element.total_dispute_cnt,
            dispute_amt:element.total_dispute_amt,
            status: element.status
          };
          if (obj.status === 'FREEZED') {
            this.TransactionsData.push(obj);
          }
      });
      

            // if (data) {
            //   this.TransactionsData = data;
            // }else{
            //     this.snackBar.open('Error! Try again.', 'Dismiss', {
            //     duration: 8000,
            //     horizontalPosition: 'center',
            //     verticalPosition: 'top'
            //   });
            // }
              
        })
        .catch((err) => {
          globals.loaderOff();
          this.snackBar.open('Error! Try again.', 'Dismiss', {
            duration: 8000,
            horizontalPosition: 'center',
            verticalPosition: 'top'
          });
        });
   
    

    // Initialize Ledgers
    // this.ledgerService.callLedgerQuery().then((data: Array < Object > ) => {
    //   this.ELEMENT_DATA = [];
    //   console.log(data, 'data called?');
    //   data.forEach((element: any) => {
       
    //     const date = new Date(parseInt(element.inserted_date));

    //     const obj: any = {
    //       ledger_uid: element.ledger_uid,
    //       created_on: '' + date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear(),
    //       total_transaction: element.total_tnx,
    //       cash: element.received_amt_cash + '/' + element.total_cash_cnt,
    //       cheque: element.received_amt_chq + '/' + element.total_cheq_cnt,
    //       card: element.received_amt_card + '/' + element.total_card_cnt,
    //       status: element.status
    //     };
    //     if (obj.status === 'FREEZED') {
    //       this.ELEMENT_DATA.push(obj);
    //     }
    //   });
    //   this.dataSource = new MatTableDataSource < LedgerElement > (this.ELEMENT_DATA);
    //   this.dataSource.paginator = this.paginator;
    //   this.dataSource.sort = this.sort;
    // });
    

    this.globals.loaderOff();

  }

  ngOnInit() {}

  // applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }

  openTransactions(ledgeruid: string, created_on: any) {
    this.globals.loaderOn();
    this.router.navigate(['/mods//ledger/summary'], {
      state: {
        ledger_uid: ledgeruid,
        created_on: created_on
      }
    });
  }

}

// export interface LedgerElement {
//   ledger_uid: string;
//   created_on: string;
//   total_transaction: number;
//   cash: string;
//   cheque: string;
//   card: string;
//   status: string;
// }
