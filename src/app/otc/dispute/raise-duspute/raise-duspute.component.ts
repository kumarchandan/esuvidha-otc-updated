import { Component, OnInit } from "@angular/core";

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router, NavigationEnd } from "@angular/router";
import { MatIconRegistry, MatSnackBar, MatDialog } from "@angular/material";

import { LedgerService } from "src/app/services/ledger.service";
import { GlobalService } from "src/app/services/global/global";

import { DisputeComponent } from "../../ledger/openned-ledger/dispute/dispute.component";

var converter = require("number-to-words");

@Component({
  selector: "app-raise-duspute",
  templateUrl: "./raise-duspute.component.html",
  styleUrls: ["./raise-duspute.component.scss"]
})
export class RaiseDusputeComponent implements OnInit {
  tnx_data;
  counter_no;
  fetch: FormGroup;

  submitted = false;
  view_tnx = false;

  constructor(
    private ledgerService: LedgerService,
    private router: Router,
    private global: GlobalService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.fetch = new FormGroup({
      fetch_tnx_uid: new FormControl(null, Validators.required)
    });

    global.loaderOff();
  }

  ngOnInit() {}

  onPrint() {
    window.print();
  }

  back() {
    this.fetch = new FormGroup({
      fetch_tnx_uid: new FormControl(null, Validators.required)
    });
    this.view_tnx = false;
  }

  submit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.fetch.invalid) {
      this.snackBar.open("Error! Enter a valid ID!", "Dismiss", {
        duration: 8000,
        horizontalPosition: "center",
        verticalPosition: "top"
      });
      return;
    }

    this.global.loaderOn();
    var fetch_id = this.fetch.controls.fetch_tnx_uid.value.toString();

    this.ledgerService
      .fetchcompleteTnxDtls(fetch_id)
      .then(data => {
        console.log("------------ selected tnx data ---------------", data.dat);
        this.tnx_data = data.dat;
        this.tnx_data.in_words = `${converter.toWords(
          this.tnx_data.paid_amt
        )} only`;
        this.tnx_data.amtInWords =
          this.tnx_data.in_words.charAt(0).toUpperCase() +
          this.tnx_data.in_words.slice(1);
        // this.tnx_data.in_words = '';
        this.tnx_data.username = sessionStorage.getItem("username");

        var cnt = data.dat.counter_no;
        this.counter_no = cnt.slice(cnt.length - 2, cnt.length);
        this.view_tnx = true;
        this.global.loaderOff();
      })
      .catch(err => {
        this.global.loaderOff();
        this.snackBar.open("Error! Try again.", "Dismiss", {
          duration: 8000,
          horizontalPosition: "center",
          verticalPosition: "top"
        });
      });
  }

  getAddress() {
    let obj = {
      addressLine: "",
      city: ""
    };
    let address = JSON.parse(this.tnx_data.address);
    console.log("------------------------------ ", address);
    obj.addressLine = `${address.AddressLine1} ${"-"} ${address.PinCode}`;
    console.log("------------------------------------", obj);
    return obj.addressLine;
  }

  getReceiptDate() {
    const receiptDate = new Date();
    console.log(receiptDate);
    const r =
      receiptDate.getDate() +
      "/" +
      (receiptDate.getMonth() + 1) +
      "/" +
      receiptDate.getFullYear();
    return r;
  }

  getReceiptTime() {
    const receiptTime = new Date();
    console.log("------------", receiptTime);
    return receiptTime.toLocaleString("en-US", {
      hour: "numeric",
      minute: "numeric",
      hour12: true
    });
  }

  get f() {
    return this.fetch.controls;
  }

  //-------------------------- Dispute ---------------------------
  openDisputeDialog() {
    this.global.loaderOn();
    console.log("------ in dispute -----");

    if (
      this.tnx_data.tnx_status_name == "PAYMENT INITIATED" ||
      this.tnx_data.tnx_status_name == "RETRY"
    ) {
      this.ledgerService
        .getPaymentModeDetails(
          this.tnx_data.tnx_uid,
          this.tnx_data.payment_mode
        )
        .then(data => {
          if (data.status == "F") {
            var dialog_data = {
              ledger_uid: this.tnx_data.ledger_uid,
              tnx_uid: this.tnx_data.tnx_uid,
              bill_amt: this.tnx_data.bill_amt,
              assigned_to:
                this.tnx_data.fname +
                " " +
                this.tnx_data.mname +
                " " +
                this.tnx_data.lname,
              assigned_to_uid: sessionStorage.getItem("agent_uid"),
              status_code: this.tnx_data.tnx_status_name,
              txn_status_name: this.tnx_data.tnx_status_name,
              consumer_acct: this.tnx_data.con_acct_id,
              consumer_ph: this.tnx_data.con_contact_num,
              paid_amt: this.tnx_data.paid_amt,
              tnx_date: this.tnx_data.inserted_date,
              payment_mode: this.tnx_data.payment_mode,
              created_on: this.tnx_data.inserted_date,
              received_amount: data.dat.received_amount,
              paid_amount: data.dat.paid_amount,
              //return_amount: data.dat.return_amount,
              card_type: data.dat.card_type,
              pos_num: data.dat.pos_num,
              cheque_num: data.dat.cheque_num,
              bank_name: data.dat.bank_name,
              cheq_due_date: new Date(Number(data.dat.cheq_due_date)),
              mode_type: data.dat.mode_type,
              rtgs_num: data.dat.rtgs_num,
              neft_num: data.dat.neft_num
            };

            console.log(dialog_data);

            const dialogRef = this.dialog.open(DisputeComponent, {
              width: "1300px",
              height: "380px",
              data: dialog_data
            });

            dialogRef.afterClosed().subscribe(result => {
              console.log("The dialog was closed :=> ", result);

              if (result == undefined) {
                result = {};
                result.status = "NO";
              } else if (result.status == "YES") {
                if (
                  result.pos_id == null ||
                  result.pos_id == "" ||
                  result.pos_id == undefined
                ) {
                  result.pos_id = " ";
                }

                var dispute_data = {
                  tnx_uid: dialog_data.tnx_uid,
                  remark: result.remarks,
                  ledger_uid: dialog_data.ledger_uid,
                  dispute_amt: dialog_data.paid_amt,
                  assigned_to: dialog_data.assigned_to_uid
                  // amt_return_mode: result.amt_return_mode,
                  // pos_number: result.pos_id,
                  // cheque_number: result.cheque_no
                };
                //console.log("-------- filing data -------", dispute_data);
                this.ledgerService
                  .fileDispute(dispute_data)
                  .then(data => {
                    console.log(" --------- dispute filed----------- ", data);
                    if (data.status == "F") {
                      this.global.loaderOff();
                      this.snackBar.open("Dispute has been filed!", "Dismiss", {
                        duration: 8000,
                        horizontalPosition: "center",
                        verticalPosition: "top"
                      });
                      this.router.navigate([
                        "/mods/ledger/oledger/dispute/dispute-receipt"
                      ]);
                      //this.router.navigate(['/mods/ledger/oledger']);
                    }
                  })
                  .catch(err => {
                    this.global.loaderOff();
                    this.snackBar.open("Error! Try again.", "Dismiss", {
                      duration: 8000,
                      horizontalPosition: "center",
                      verticalPosition: "top"
                    });
                  });
              } else {
                this.global.loaderOff();
              }
            });
          }
        })
        .catch(err => {
          this.global.loaderOff();
          this.snackBar.open("Error! Try again.", "Dismiss", {
            duration: 8000,
            horizontalPosition: "center",
            verticalPosition: "top"
          });
        });
    } else {
      this.global.loaderOff();
      this.snackBar.open("Can not raise dispute", "Dismiss", {
        duration: 8000,
        horizontalPosition: "center",
        verticalPosition: "top"
      });
      return;
    }
  }
}
