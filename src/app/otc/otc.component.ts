import {
  Component,
  OnInit,
  AfterViewInit,
  ElementRef,
  ViewChild,
  VERSION
} from '@angular/core';
import {
  NavService
} from 'src/app/services/global/nav.service';
import {
  Idle,
  DEFAULT_INTERRUPTSOURCES
} from '@ng-idle/core';
import {
  NavItem
} from '../components/nav/nav-items';
import {
  AuthService
} from '../services/global/auth.service';
import {
  HAMMER_LOADER
} from '@angular/platform-browser';
import {
  GlobalService
} from '../services/global/global';
import {
  Router
} from '@angular/router';
import {
  MatSnackBar
} from '@angular/material';

@Component({
  selector: 'app-otc',
  templateUrl: './otc.component.html',
  styleUrls: ['./otc.component.scss']
})
export class OtcComponent implements AfterViewInit {

  @ViewChild('appDrawer', {
    read: '',
    static: true
  }) appDrawer: ElementRef;
  version = VERSION;
  navItems: NavItem[] = [{
      displayName: 'Dashboard',
      iconName: 'dashboard',
      route: 'mods/dashboard'
    },
    {
      displayName: 'Ledger',
      iconName: 'account_balance_wallet',
      route: 'mods/ledger',
      children: [{
          displayName: 'Opened Ledger',
          iconName: 'add_box',
          route: 'mods/ledger/oledger',
        },
        {
          displayName: 'Freezed Ledgers',
          iconName: 'archive',
          route: 'mods/ledger/fledger',
        }
      ]
    },
    {
      displayName: 'View Transaction',
      iconName: 'calendar_view_day',
      route: 'mods/vtransaction',
    },
    {
      displayName: 'Raise Dispute',
      iconName: 'autorenew',
      route: 'mods/any-dispute',
    },
    {
      displayName: 'New Transaction',
      iconName: 'add_box',
      route: 'mods/newtransaction',
      divider: true
    },
    {
      displayName: 'Ticket',
      iconName: 'add_box',
      route: 'mods/newtransaction',
      divider: true,
      children: [{
          displayName: 'Create Ticket',
          iconName: 'add_box',
          route: 'mods/ticket/CreateTicket',
        },
        {
          displayName: 'View Ticket',
          iconName: 'archive',
          route: 'mods/ticket/ViewTicket',
        }
      ]
    },
    {
      displayName: 'Appointments',
      iconName: 'add_box',
      route: 'mods/Appointments',
      divider: true,
      children: [{
          displayName: 'View Appointment',
          iconName: 'archive',
          route:'mods/Appointments/viewAppointment',
        },

      ]
    }
  ];

  constructor(private navService: NavService, private idle: Idle, private auth: AuthService,
    private global: GlobalService, router: Router, private snackBar: MatSnackBar) {

    auth.userOnline()
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      });

    // sets an idle timeout of 5 seconds, for testing purposes.
    idle.setIdle(900);

    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    idle.setTimeout(10);

    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.watch();

    idle.onTimeout.subscribe(() => {

      this.global.loaderOn();

      this.auth.logout().then((data) => {
          this.snackBar.open('You have been logged-out.', 'Dismiss', {
            duration: 8000,
          });
          sessionStorage.clear();
          router.navigate(['/']);
        })
        .catch((error) => {
          router.navigate(['/']);
        });
    });
  }

  ngAfterViewInit() {
    this.navService.appDrawer = this.appDrawer;
  }

}