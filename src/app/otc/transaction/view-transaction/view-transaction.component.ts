import { Component, OnInit } from "@angular/core";

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router, NavigationEnd } from "@angular/router";
import { MatIconRegistry, MatSnackBar, MatDialog } from "@angular/material";

import { LedgerService } from "src/app/services/ledger.service";
import { GlobalService } from "src/app/services/global/global";

var converter = require("number-to-words");

@Component({
  selector: "app-view-transaction",
  templateUrl: "./view-transaction.component.html",
  styleUrls: ["./view-transaction.component.scss"]
})
export class ViewTransactionComponent implements OnInit {
  tnx_data;
  counter_no;
  fetch: FormGroup;

  submitted = false;
  view_tnx = false;

  constructor(
    private ledgerService: LedgerService,
    private router: Router,
    private global: GlobalService,
    private snackBar: MatSnackBar
  ) {
    this.fetch = new FormGroup({
      fetch_tnx_uid: new FormControl(null, Validators.required)
    });

    global.loaderOff();
  }

  ngOnInit() {}

  onPrint() {
    window.print();
  }

  back() {
    this.fetch = new FormGroup({
      fetch_tnx_uid: new FormControl(null, Validators.required)
    });
    this.view_tnx = false;
  }

  submit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.fetch.invalid) {
      this.snackBar.open("Error! Enter a valid ID!", "Dismiss", {
        duration: 8000,
        horizontalPosition: "center",
        verticalPosition: "top"
      });
      return;
    }

    this.global.loaderOn();
    var fetch_id = this.fetch.controls.fetch_tnx_uid.value.toString();

    this.ledgerService
      .fetchcompleteTnxDtls(fetch_id)
      .then(data => {
        console.log("------------ selected tnx data ---------------", data.dat);
        this.tnx_data = data.dat;
        this.tnx_data.in_words = `${converter.toWords(
          this.tnx_data.paid_amt
        )} only`;
        this.tnx_data.amtInWords =
          this.tnx_data.in_words.charAt(0).toUpperCase() +
          this.tnx_data.in_words.slice(1);
        // this.tnx_data.in_words = '';
        this.tnx_data.username = sessionStorage.getItem("username");

        var cnt = data.dat.counter_no;
        this.counter_no = cnt.slice(cnt.length - 2, cnt.length);
        this.view_tnx = true;
        this.global.loaderOff();
      })
      .catch(err => {
        this.global.loaderOff();
        this.snackBar.open("Error! Try again.", "Dismiss", {
          duration: 8000,
          horizontalPosition: "center",
          verticalPosition: "top"
        });
      });
  }

  getAddress() {
    let obj = {
      addressLine: "",
      city: ""
    };
    let address = JSON.parse(this.tnx_data.address);
    console.log("------------------------------ ", address);
    obj.addressLine = `${address.AddressLine1} ${"-"} ${address.PinCode}`;
    console.log("------------------------------------", obj);
    return obj.addressLine;
  }

  getReceiptDate() {
    const receiptDate = new Date();
    console.log(receiptDate);
    const r =
      receiptDate.getDate() +
      "/" +
      (receiptDate.getMonth() + 1) +
      "/" +
      receiptDate.getFullYear();
    return r;
  }

  getReceiptTime() {
    const receiptTime = new Date();
    console.log("------------", receiptTime);
    return receiptTime.toLocaleString("en-US", {
      hour: "numeric",
      minute: "numeric",
      hour12: true
    });
  }

  get f() {
    return this.fetch.controls;
  }
}
