// tslint:disable:prefer-const
import {
  Component,
  OnInit
} from '@angular/core';
import {
  LedgerService
} from 'src/app/services/ledger.service';
import {
  Router,
  NavigationEnd
} from '@angular/router';
import {
  AuthService
} from 'src/app/services/global/auth.service';
import {
  GlobalService
} from 'src/app/services/global/global';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.scss']
})
export class ReceiptComponent implements OnInit {

  public receiptData;
  public paymentData;
  public totalBillAmount;

  fullName = sessionStorage.getItem('FullName');

  agent_name = sessionStorage.getItem('username');
  centre_code = sessionStorage.getItem('centre_code');
  centre_name = sessionStorage.getItem('centre_name');
  counter_name = sessionStorage.getItem('counter_no');
  counter_no = this.counter_name.slice(this.counter_name.length - 2, this.counter_name.length);
  current_date = new Date();

  constructor(private ledgerService: LedgerService,
    private authService: AuthService,
    private router: Router,
    public global: GlobalService,
  ) {
    this.receiptData = JSON.parse(sessionStorage.getItem('fetched_bill'));
    this.paymentData = JSON.parse(sessionStorage.getItem('paymentDtl'));

    sessionStorage.setItem('tnx_status', 'C');

    this.global.loaderOff();
  }

  ngOnInit() {
    console.log('receipt data:', this.receiptData);
    this.totalBillAmount = Math.round(this.receiptData.bill_amt)
  }

  getAddress() {
    let obj = {
      addressLine: '',
      city: ''
    };
    let address = JSON.parse(this.receiptData.address);
    console.log('------------------------------ ', address);
    obj.addressLine = address.AddressLine1;
    // let intrim_city = address.City.split(' ');
    // obj.city = intrim_city[0];
    console.log('------------------------------------', obj);
    return obj;
  }

  getReceiptDate() {
    const receiptDate = new Date();
    console.log(receiptDate);
    const r = receiptDate.getDate() + '/' + (receiptDate.getMonth() + 1) + '/' + receiptDate.getFullYear();
    return r;
  }

  getReceiptTime() {
    const receiptTime = new Date(this.paymentData.inserted_date)
    console.log('------------',receiptTime)
    return receiptTime.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
  }

  onPrint() {
    window.print();
  }

}
