import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';

import {
  DomSanitizer
} from '@angular/platform-browser';
import {
  MatIconRegistry,
  MatSnackBar,
  MatDialog
} from '@angular/material';

import {
  NgxUiLoaderService
} from 'ngx-ui-loader';
import {
  Router,
  NavigationEnd
} from '@angular/router';

// import { EditTransactionComponent } from './edit-transaction/edit-transaction.component';

import {
  LedgerService
} from 'src/app/services/ledger.service';
import {
  GlobalService
} from 'src/app/services/global/global';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

// tslint:disable:variable-name


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit, OnDestroy {

  bill_details;

  public paidAmount;
  public receivedAmount;
  public posNumber;
  public cardType;
  public paperType;
  public chequeNumber;
  public micrNumber;
  //public bankName;
  public cheque_date;
  //public bankNameList;
  public selected = 'cash';
  public totalBillAmount;
  //public DD;
  formSubmit = false;

  cash_click = true;
  card_click = false;
  cheque_click = false;

  submitted = false;

  hide_cancel = false;

  card_type: string;

  public minDate;
  public maxDate;

  date = new FormControl(new Date());

  cancelTransactionForm: FormGroup;

  constructor(private router: Router,
    public global: GlobalService,
    // tslint:disable-next-line:align
    private snackBar: MatSnackBar,
    // tslint:disable-next-line:align
    private ledgerService: LedgerService,
    // tslint:disable-next-line:align
    iconRegistry: MatIconRegistry,
    public dialog: MatDialog,
    sanitizer: DomSanitizer) {

    this.minDate = new Date(new Date().getFullYear(), new Date().getMonth(), (new Date().getDate() - 80));
    this.maxDate = new Date(new Date().getFullYear(), new Date().getMonth(), (new Date().getDate() + 10));

    iconRegistry.addSvgIcon(
      'checkoutLogo',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/checkoutLogo.svg'));

    iconRegistry.addSvgIcon(
      'e-suvidha',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/brand-checkout.svg'));

    this.bill_details = JSON.parse(sessionStorage.getItem('fetched_bill'));
    console.log('---- in checkout ---- ', this.bill_details);

    this.cash_click = true;
    this.card_click = false;
    this.cheque_click = false;
    this.formSubmit = false;

    // this.ledgerService.bankNameDetails().then((data) => {
    //     console.log('------ bank details----- ', data.dat);
    //     this.bankNameList = data.dat;
    //   })
    //   .catch((error) => {
    //     this.snackBar.open('Couldn\'t fetch bank details.', 'Dismiss', {
    //       duration: 8000,
    //       horizontalPosition: 'center',
    //       verticalPosition: 'top'
    //     });
    //   });

    this.global.loaderOff();
  }

  ngOnInit() {
    this.paidAmount = Math.round(this.bill_details.bill_amt);
    this.totalBillAmount = Math.round(this.bill_details.bill_amt);
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event
  }

  showcancelTransaction() {

    this.submitted = false;
    this.hide_cancel = true;

    this.cancelTransactionForm = new FormGroup({
      'remark': new FormControl(null, Validators.required),
    });

    // Navigate back
    console.log('Cancel Transaction Called');
    // this.router.navigate(['/newtransaction']);
  }

  cancelTransaction() {

    this.global.loaderOn();
    this.submitted = true;
    const data = {
      tnx_uid: this.bill_details.tnx_uid,
      remark: this.cancelTransactionForm.controls.remark.value,
    };

    console.log('Cancel Transaction Field Data', data);

    this.ledgerService.cancelTnx(data).then((cncl_dat) => {
        if (cncl_dat.status === 'F') {
          this.global.loaderOff();
          this.router.navigate(['/mods/dashboard']);
          this.snackBar.open('Transaction Cancelled!', 'Dismiss', {
            duration: 8000,
            horizontalPosition: 'center',
            verticalPosition: 'top'
          });
        }
      })
      .catch((error) => {
        // this.router.navigate(['/mods/dashboard']);
        this.global.loaderOff();
        this.snackBar.open('Error occurred!', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });
  }

  getAddress() {
    const hclAddress = JSON.parse(this.bill_details.address);
    return hclAddress;
  }

  getBillDate() {
    let timestamp = this.bill_details.bill_date;
    // console.log('Timestamp x:', timestamp);
    timestamp = timestamp.replace('-', '/');
    timestamp = timestamp.replace('-', '/');
    // const d = new Date(timestamp);
    // console.log('ddddd:', d);
    // const r = '' + d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
    // console.log('bill date:', timestamp);
    return timestamp;

  }

  getBillDateHcl() {
    const timestamp = this.bill_details.bill_date;
    // console.log('Timestamp x:', timestamp);
    return timestamp;
  }

  getLastPaymentDate() {
    const timestamp = this.bill_details.last_payment_date;
    // console.log('Timestamp x:', timestamp);
    const d = new Date(timestamp);
    // console.log('d:', d);
    const r = '' + d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
    // console.log('r:', r);

    if (r.toString().includes('NaN')) {
      return 'Not available';
    }
    return r;
  }

  getReturnAmount() {
    const returnAmount = this.receivedAmount - Math.round(this.paidAmount);
    return returnAmount;
  }

  cashClicked() {

    if (this.hide_cancel === false) {
      this.cash_click = true;
      this.card_click = false;
      this.cheque_click = false;

      // -------- setting value
      this.paidAmount = Math.round(this.bill_details.bill_amt);
      this.receivedAmount = '';
      this.posNumber = '';
      this.cardType = '';
      this.paperType = '';
      this.chequeNumber = '';
      this.micrNumber = '';
      //this.bankName = '';
      this.cheque_date = '';

      this.formSubmit = false;
      this.submitted = false;
      this.hide_cancel = false;
    } else {
      return;
    }
  }

  cardClicked() {

    if (this.hide_cancel === false) {
      this.cash_click = false;
      this.card_click = true;
      this.cheque_click = false;

      // -------- setting value
      this.paidAmount = Math.round(this.bill_details.bill_amt);
      this.receivedAmount = '';
      this.posNumber = '';
      this.cardType = '';
      this.paperType = '';
      this.chequeNumber = '';
      this.micrNumber = '';
      //this.bankName = '';
      this.cheque_date = '';

      this.formSubmit = false;
      this.submitted = false;
      this.hide_cancel = false;
    } else {
      return;
    }
  }

  chequeClicked() {

    if (this.hide_cancel === false) {
      this.cash_click = false;
      this.card_click = false;
      this.cheque_click = true;
      //this.DD = true;
      // -------- setting value
      this.paidAmount = Math.round(this.bill_details.bill_amt);
      this.receivedAmount = '';
      this.posNumber = '';
      this.cardType = '';
      this.paperType = '';
      this.chequeNumber = '';
      this.micrNumber = '';
      //this.bankName = '';
      this.cheque_date = '';

      this.formSubmit = false;
      this.submitted = false;
      this.hide_cancel = false;
    } else {
      return;
    }
  }

  back() {
    this.hide_cancel = false;
  }

  postCashPayment() {
    this.ledgerService.setPaymentCash(Math.round(this.paidAmount), this.receivedAmount).then((data) => {
        if (data.status === 'F') {
          this.global.loaderOff();
          this.snackBar.open('Payment Posted!', 'Dismiss', {
            duration: 8000,
            horizontalPosition: 'center',
            verticalPosition: 'top'
          });

          sessionStorage.setItem('paymentDtl', JSON.stringify(data.dat));
          this.global.loaderOn();
          this.router.navigate(['/mods/newtransaction/checkout/receipt']);
          // sessionStorage.removeItem('fetched_bill');
        }
      })
      .catch((err) => {
        this.global.loaderOff();
        this.snackBar.open('Error occurred while payment posting.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });

    sessionStorage.setItem('tnx_status', 'C');
  }

  postCardPayment() {
    this.ledgerService.setPaymentCard(Math.round(this.paidAmount), this.posNumber, this.cardType).then((data) => {
        if (data.status === 'F') {
          this.global.loaderOff();
          this.snackBar.open('Payment Posted!', 'Dismiss', {
            duration: 8000,
            horizontalPosition: 'center',
            verticalPosition: 'top'
          });

          sessionStorage.setItem('paymentDtl', JSON.stringify(data.dat));
          this.global.loaderOn();
          this.router.navigate(['/mods/newtransaction/checkout/receipt']);
          // sessionStorage.removeItem('fetched_bill');
        }
      })
      .catch((err) => {
        this.global.loaderOff();
        this.snackBar.open('Error occurred while payment posting.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });

    sessionStorage.setItem('tnx_status', 'C');
  }

  postChequePayment() {
    console.log(this.chequeNumber);
    if (this.chequeNumber.length !== 6) {
      this.snackBar.open('Cheque Number is invalid', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
      this.global.loaderOff();
      return;
    }
    if (this.micrNumber.length !== 9) {
      this.snackBar.open('Micr code is invalid', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
      this.global.loaderOff();
      return;
    }
    console.log('*** paper payment category ***',this.paperType)
    this.ledgerService.setPaymentCheque(Math.round(this.paidAmount), this.chequeNumber, this.micrNumber, this.cheque_date, this.paperType).then((data) => {
      console.log('***== paper payment category ==***',this.paperType)
        if (data.status === 'F') {
          this.global.loaderOff();
          this.snackBar.open('Payment Posted!', 'Dismiss', {
            duration: 8000,
            horizontalPosition: 'center',
            verticalPosition: 'top'
          });

          sessionStorage.setItem('paymentDtl', JSON.stringify(data.dat));
          this.global.loaderOn();
          this.router.navigate(['/mods/newtransaction/checkout/receipt']);
          // sessionStorage.removeItem('fetched_bill');
        }
      })
      .catch((err) => {
        this.global.loaderOff();
        this.snackBar.open('Error occurred while payment posting.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });

    sessionStorage.setItem('tnx_status', 'C');
  }

  doPayment() {
    this.formSubmit = true;
    this.global.loaderOn();
    if (Math.round(this.paidAmount) > Math.round(this.bill_details.bill_amt)) {
      this.snackBar.open('Partial payments not allowed.', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
      this.global.loaderOff();
      return;
    }

    if (this.cash_click === true) {
      this.postCashPayment();
    } else if (this.cheque_click === true) {
      this.postChequePayment();
    } else if (this.card_click === true) {
      this.postCardPayment();
    } else {
      this.global.loaderOff();
      this.snackBar.open('Error! Please recheck the input form fields.', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    }
  }

  // openEditTransaction(){

  //   var dialog_data = {
  //     acct_id : this.getDetails().account_no,
  //     phone_no : this.getDetails().contact_no,
  //     region : this.getDetails().region,
  //     tnx_uid: this.getDetails().tnx_uid,
  //     service: this.getDetails().service_name
  //   }

  //   const dialogRef = this.dialog.open(EditTransactionComponent, {
  //     width: '350px',
  //     height: '380px',
  //     data: dialog_data
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log(" --- dialog closed ---- ", result);

  //     if(result.editType == 'MOBILE'){
  //       this.getDetails().contact_no = result.newVal;
  //     }
  //     else if(result.editType == 'ACCT'){
  //       this.ngxService.start();
  //       setTimeout( () => {
  //         this.initialiseInvites();
  //       }, 1500)

  //     }
  //   })
  // }

  get f() {
    return this.cancelTransactionForm.controls;
  }

}
