// tslint:disable:class-name

import {
  Component,
  OnInit
} from '@angular/core';

import {
  Router,
  NavigationEnd,
  ActivatedRoute
} from '@angular/router';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  MatIconRegistry,
  MatSnackBar
} from '@angular/material';

import {
  GlobalService
} from 'src/app/services/global/global';
import {
  AuthService
} from 'src/app/services/global/auth.service';
import {
  LedgerService
} from 'src/app/services/ledger.service';

export interface billType {
  value: string;
  viewValue: string;
}

export interface regionType {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-new-transaction',
  templateUrl: './new-transaction.component.html',
  styleUrls: ['./new-transaction.component.scss']
})
export class NewTransactionComponent implements OnInit {

  enable_edit = false;
  content_hide = false;

  newTransactionForm: FormGroup;
  submitted = false;

  billTypes: billType[] = [{
      value: 'ELB',
      viewValue: 'Electricity'
    },
    // {value: 'Wtr', viewValue: 'Water'}
  ];

  regions: regionType[] = [{
      value: 'URBAN',
      viewValue: 'Urban'
    },
    {
      value: 'RURAL',
      viewValue: 'Rural'
    }
  ];

  edit_acct_cnt = 0;
  edit_mob_cnt = 0;

  edit_acct_val = '';
  edit_mob_val = '';

  constructor(
    public global: GlobalService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private ledgerService: LedgerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    const ledgerStat = JSON.parse(sessionStorage.getItem('oledger_uid'));
    if (ledgerStat.led_status_name === 'OPEN') {
      global.loaderOff();
      this.newTransactionForm = new FormGroup({
        'consumer_id': new FormControl(null, [
          Validators.required,
          Validators.pattern('[0-9]*')
        ]),
        'bill_type': new FormControl(null, Validators.required),
        'region': new FormControl(null, Validators.required),
        'mobile_number': new FormControl('', [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern('[0-9]*')
        ])
      });
    } else {
      global.loaderOff();
      this.snackBar.open('Can only transact once a Ledger has been opened.', 'Dismiss', {
        duration: 20000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
      this.content_hide = true;
    }
  }

  ngOnInit() {
    this.global.loaderOff();
  }

  next() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.newTransactionForm.invalid) {
      this.global.loaderOff();
      return;
    } else {
      this.enable_edit = true;
      this.newTransactionForm.disable();
    }
  }

  AcctID() {
    if ((this.edit_acct_cnt) % 2 === 0) {
      this.edit_acct_val = this.newTransactionForm.controls.consumer_id.value;
      this.newTransactionForm.controls['consumer_id'].enable();
      this.edit_acct_cnt++;
    } else {
      this.newTransactionForm.controls['consumer_id'].disable();
      this.edit_acct_cnt++;
    }
  }

  Mobile() {
    if ((this.edit_mob_cnt) % 2 === 0) {
      this.edit_mob_val = this.newTransactionForm.controls.mobile_number.value;
      this.newTransactionForm.controls['mobile_number'].enable();
      this.edit_mob_cnt++;
    } else {
      this.newTransactionForm.controls['mobile_number'].disable();
      this.edit_mob_cnt++;
    }

  }

  cancel() {
    console.log('-------------- in cancel-----------');
    this.router.navigate(['/mods/dashboard']);
    this.snackBar.open('Transaction Cancelled', 'Dismiss', {
      duration: 8000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
    this.global.loaderOn();
  }

  submit() {

    this.global.loaderOn();
    this.submitted = true;
    // stop here if form is invalid
    if (this.newTransactionForm.invalid) {
      this.global.loaderOff();
      return;
    } else {
      this.ledgerService.createNewTransaction().then((data) => {
          console.log(data.dat);
          const fetch_data = {
            con_acct_id: this.newTransactionForm.controls.consumer_id.value,
            contact_num: this.newTransactionForm.controls.mobile_number.value,
            service_code: this.newTransactionForm.controls.bill_type.value,
            region: this.newTransactionForm.controls.region.value,
            tnx_uid: data.dat.tnx_uid
          };

          const edit_dat = {
            tnx_uid: data.dat.tnx_uid,
            edit_key: '',
            edit_old_val: '',
            edit_new_val: '',
            remark: '',
          };

          if (this.edit_acct_cnt !== 0 || this.edit_mob_cnt !== 0) {
            if (this.edit_acct_val !== '' && this.edit_acct_val !== fetch_data.con_acct_id) {
              edit_dat.edit_key = 'ACCOUNT_ID';
              edit_dat.edit_old_val = this.edit_acct_val.toString();
              edit_dat.edit_new_val = fetch_data.con_acct_id.toString();
              edit_dat.remark = 'ACCT_ID_EDIT';
            } else if (this.edit_mob_val !== '' && this.edit_mob_val !== fetch_data.contact_num) {
              edit_dat.edit_key = 'CONTACT_NUMBER';
              edit_dat.edit_old_val = this.edit_mob_val.toString();
              edit_dat.edit_new_val = fetch_data.contact_num.toString();
              edit_dat.remark = 'CONTACT_NUMBER_EDIT';
            }

            this.ledgerService.editTnx(edit_dat).then((datas) => {
                console.log('------------- edit transaction: data -------------- ', datas.status);
              })
              .catch((err) => {
                console.log('------------- edit transaction: error -------------- ', err);
              });

          }

          console.log('New Transaction Field Data', fetch_data);

          this.ledgerService.fetchBill(fetch_data).then((bill_data) => {
              if (bill_data.status === 'E') {
                this.router.navigate(['/mods/dashboard']);
                this.snackBar.open(bill_data.dat, 'Dismiss', {
                  duration: 8000,
                  horizontalPosition: 'center',
                  verticalPosition: 'top'
                });
              } else if (bill_data.status === 'F') {

                sessionStorage.setItem('fetched_bill', JSON.stringify(bill_data.dat));
                console.log('---- fetched -------- ', sessionStorage.getItem('fetched_bill'));
                sessionStorage.setItem('tnx_status', 'P');
                this.router.navigate(['/mods/newtransaction/checkout']);
              }
            })
            .catch((error) => {
              if (error.status === 'E') {
                this.global.loaderOff();
                console.log(error);
                const error_msg_ntwrk = error.dat.split('Network error');
                if (error_msg_ntwrk === undefined || error_msg_ntwrk.length < 1) {
                  this.router.navigate(['/mods/newtransaction']);
                  this.snackBar.open('Network Timed-out! Try agin.', 'Dismiss', {
                    duration: 8000,
                    horizontalPosition: 'center',
                    verticalPosition: 'top'
                  });
                } else {
                  const error_msg = error.dat.split('{ message: "');
                  const err_msg = error_msg[1].split('", locations:');
                  // error_msg = JSON.parse(error_msg[1]);
                  console.log('------ error in fetch bill--------', err_msg);

                  this.router.navigate(['/mods/newtransaction']);
                  this.snackBar.open(err_msg[0], 'Dismiss', {
                    duration: 8000,
                    horizontalPosition: 'center',
                    verticalPosition: 'top'
                  });
                }
              } else if (error.status === 'F') {
                this.global.loaderOff();
                console.log(error.dat);
                const error_msg = error.dat.split('{ message: "');
                const err_msg = error_msg[1].split('", locations:');
                // error_msg = JSON.parse(error_msg[1]);
                console.log('------ error in fetch bill--------', err_msg);

                this.router.navigate(['/mods/newtransaction']);
                this.snackBar.open(err_msg[0], 'Dismiss', {
                  duration: 8000,
                  horizontalPosition: 'center',
                  verticalPosition: 'top'
                });
              }
            });
        })
        .catch((err) => {
          if (err.status === 'E') {
            this.router.navigate(['/mods/dashboard']);
            this.snackBar.open('Error occurred. Try Again.', 'Dismiss', {
              duration: 8000,
              horizontalPosition: 'center',
              verticalPosition: 'top'
            });
          }
        });
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.newTransactionForm.controls;
  }

}
