import {
  NgModule
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  OtcComponent
} from './otc.component';
import {
  OtcRoutingModule
} from './otc-routing.module';
import {
  NavComponent
} from '../components/nav/nav.component';
import {
  NgSelectModule
} from '@ng-select/ng-select';
import {
  MenuListItemComponent
} from '../components/nav/menu-list-item/menu-list-item.component';
import {
  MaterialModule
} from '../material.module';
import {
  DashboardComponent
} from './dashboard/dashboard.component';
import {
  FreezedLedgerComponent
} from './ledger/freezed-ledger/freezed-ledger.component';
import {
  OpennedLedgerComponent
} from './ledger/openned-ledger/openned-ledger.component';
import {
  ViewTransactionComponent
} from './transaction/view-transaction/view-transaction.component';
import {
  NewTransactionComponent
} from './transaction/new-transaction/new-transaction.component';
import {
  RaiseDusputeComponent
} from './dispute/raise-duspute/raise-duspute.component';
import {
  CheckoutComponent
} from './transaction/new-transaction/checkout/checkout.component';
import {
  ReceiptComponent
} from './transaction/new-transaction/checkout/receipt/receipt.component';
import {
  DenominationsComponent
} from './denominations/denominations.component';
import {
  DenominationsReceiptComponent
} from './denominations/denominations-receipt/denominations-receipt.component';
import {
  DisputeComponent
} from './ledger/openned-ledger/dispute/dispute.component';
import {
  DisputeReceiptComponent
} from './ledger/openned-ledger/dispute/dispute-receipt/dispute-receipt.component';

import {
  ReactiveFormsModule,
  FormsModule
} from '@angular/forms';
import {
  DataTablesModule
} from 'angular-datatables';
import {
  LedgerSummaryComponent
} from './ledger/ledger-summary/ledger-summary.component';

import { CreateTicketComponent } from './ticket/create-ticket/create-ticket.component';
import { ViewTicketComponent } from './ticket/view-ticket/view-ticket.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { ViewAppointmentComponent } from './appointment/view-appointment/view-appointment.component';

@NgModule({
  declarations: [
    OtcComponent,
    NavComponent,
    MenuListItemComponent,
    DashboardComponent,
    FreezedLedgerComponent,
    OpennedLedgerComponent,
    ViewTransactionComponent,
    NewTransactionComponent,
    RaiseDusputeComponent,
    CheckoutComponent,
    ReceiptComponent,
    LedgerSummaryComponent,
    DenominationsComponent,
    DenominationsReceiptComponent,
    DisputeComponent,
    DisputeReceiptComponent,
    ViewAppointmentComponent,
    CreateTicketComponent,
    ViewTicketComponent,
 
  ],
  imports: [
    CommonModule,
    OtcRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    DataTablesModule,
    MaterialModule,
    NgSelectModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot()
     
  ],
  entryComponents: [
    DisputeComponent,
  ]
})
export class OtcModule {}
