import { Component, OnInit, ViewChild, OnDestroy, OnChanges, AfterViewInit } from '@angular/core';

import { DomSanitizer } from '@angular/platform-browser';
import {
  MatIconRegistry,
  MatSnackBar
} from '@angular/material';

import {
  GlobalService
} from '../../services/global/global';
import {
  AuthService
} from '../../services/global/auth.service';
import {
  LedgerService
} from '../../services/ledger.service';
import { Router, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-denominations',
  templateUrl: './denominations.component.html',
  styleUrls: ['./denominations.component.scss']
})
export class DenominationsComponent implements OnInit, OnDestroy {

  public rs_2000 = 0;
  public rs_500 = 0;
  public rs_200 = 0;
  public rs_100 = 0;
  public rs_50 = 0;
  public rs_20 = 0;
  public rs_10 = 0;
  public rs_5 = 0;
  public rs_2 = 0;
  public rs_1 = 0;

  public closedLD;

  disable_freeze = false;
  disallow_freeze = false;

  denomination_sum = 0;
  step = 0;

  agent_name = sessionStorage.getItem('username');
  fullname = sessionStorage.getItem('FullName');
  centre_name = sessionStorage.getItem('centre_name');
  centre_code = sessionStorage.getItem('centre_code');
  counter_name = sessionStorage.getItem('counter_no');
  closedLD_id = JSON.parse(sessionStorage.getItem('oledger_uid'));
  counter_no = this.counter_name.slice(this.counter_name.length - 2, this.counter_name.length);

  current_date = new Date();

  constructor(private router: Router,
    public global: GlobalService,
    private authService: AuthService,
    private ledgerService: LedgerService,
    private snackBar: MatSnackBar,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer) {

    if (this.closedLD_id.led_status_name === 'FREEZED') {
      this.disable_freeze = true;
      this.disallow_freeze = false;
    } else if (this.closedLD_id.led_status_name === 'CLOSED') {
      this.disable_freeze = false;
      this.disallow_freeze = false;
    } else {
      this.disallow_freeze = true;
      this.disable_freeze = false;
    }

    this.ledgerService.fetchClosedLedgerQuery(this.closedLD_id.ledger_uid, 'CLOSED').then((data) => {
      this.global.loaderOff();
      if (data.status === 'F') {
        this.closedLD = data.dat;
        console.log('------ closed ledger : in denomination ----- ', this.closedLD);
      } else {
        this.snackBar.open('Closed Ledger details found empty. Either, ledger might not have been closed or, Try Again, later.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      }
    })
    .catch((error) => {
      this.global.loaderOff();
      this.snackBar.open('Error. Retry.', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  calc_denomination() {

    console.log(' ----- dino ----- ', this.denomination_sum);
    this.denomination_sum = ((2000 * this.rs_2000) + (500 * this.rs_500) + (200 * this.rs_200) + (100 * this.rs_100) + (50 * this.rs_50) + (20 * this.rs_20) + (10 * this.rs_10) + (5 * this.rs_5) + (2 * this.rs_2) + (1 * this.rs_1));

  }

  freezeLedger() {
    this.global.loaderOn();
    console.log('Freeze Ledger called');

    this.ledgerService.freezeLedger(this.rs_2000, this.rs_500, this.rs_200, this.rs_100, this.rs_50, this.rs_20, this.rs_10, this.rs_5, this.rs_2, this.rs_1, this.closedLD, this.closedLD_id.ledger_uid).then((data) => {
      this.global.loaderOff();
      if (data.status === 'F') {
        this.snackBar.open('Ledger Freezes with UID ' + this.closedLD_id.ledger_uid + '!', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });

        this.global.loaderOn();
        this.router.navigate(['/mods/settle-denomination/denominations-receipt']);
      }
    })
    .catch((error) => {
      this.global.loaderOff();
      this.snackBar.open('Error. Retry.', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    });
  }

}
