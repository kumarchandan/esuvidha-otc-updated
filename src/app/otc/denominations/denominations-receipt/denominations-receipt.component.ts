import {
  Component,
  OnInit
} from '@angular/core';

import {
  MatIconRegistry,
  MatSnackBar
} from '@angular/material';

import {
  GlobalService
} from 'src/app/services/global/global';
import {
  AuthService
} from 'src/app/services/global/auth.service';
import {
  LedgerService
} from 'src/app/services/ledger.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-denominations-receipt',
  templateUrl: './denominations-receipt.component.html',
  styleUrls: ['./denominations-receipt.component.scss']
})

export class DenominationsReceiptComponent implements OnInit {

  public closedLD;
  public freezedLD;
  public cashPrint = true;
  public cardPrint = true;
  public chequePrint = true;

  agent_name = sessionStorage.getItem("username");
  fullname = sessionStorage.getItem("FullName");
  centre_code = sessionStorage.getItem("centre_code");
  counter_name = sessionStorage.getItem("counter_no");
  closedLD_id = JSON.parse(sessionStorage.getItem("oledger_uid"));
  counter_no = this.counter_name.slice(this.counter_name.length - 2, this.counter_name.length);

  constructor(
    private router: Router,
    public global: GlobalService,
    private authService: AuthService,
    private ledgerService: LedgerService,
    private snackBar: MatSnackBar,
    iconRegistry: MatIconRegistry,
  ) {

    this.ledgerService.fetchClosedLedgerQuery(this.closedLD_id.ledger_uid, 'FREEZED').then((data) => {
      this.global.loaderOff();
      if(data.status == 'F'){
        this.closedLD = data.dat;
        console.log("------ closed ledger : in denomination ----- ", this.closedLD)
      }
      else{
        this.snackBar.open('Closed Ledger details found empty. Either, ledger might not have been closed or, Try Again, later.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      }
    })
    .catch((error) => {
      this.global.loaderOff();
      this.snackBar.open('Error. Retry.', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    });

    this.ledgerService.fetchFreezedLedgerDenos(this.closedLD_id.ledger_uid, 'FREEZED').then((data) => {
      this.global.loaderOff();
      console.log("-------- freezed denos : in deno-slip ---------- ", data.dat);
      if(data.status == 'F'){
        this.freezedLD = data.dat;
      }
      else{
        this.snackBar.open('Freezed Ledger denominations found empty. Either, ledger might not have been freezed or, Try Again, later.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      }
    })
    .catch((error) => {
      this.global.loaderOff();
      this.snackBar.open('Error. Retry.', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    });
  }


  ngOnInit() {}

  getReceiptDate() {
    const receiptDate = new Date();
    console.log(receiptDate);
    const r = receiptDate.getDate() + '/' + (receiptDate.getMonth() + 1) + '/' + receiptDate.getFullYear();
    return r;
  }

  onPrint(){
    this.cashPrint = true;
    this.cardPrint = true;
    this.chequePrint = true;
    setTimeout(() => {
      this.print();
    }, 300)
  }

  oncardPrint(){
    this.cashPrint = false;
    this.cardPrint = true;
    this.chequePrint = false;
    setTimeout(() => {
      this.print();
    }, 300)
  }

  oncashPrint(){
    this.cashPrint = true;
    this.cardPrint = false;
    this.chequePrint = false;
    setTimeout(() => {
      this.print();
    }, 300)
  }

  onchequePrint(){
    this.cashPrint = false;
    this.cardPrint = false;
    this.chequePrint = true;
    setTimeout(() => {
      this.print();
    }, 300)
  }

  print(){
    window.print();
  }

}
