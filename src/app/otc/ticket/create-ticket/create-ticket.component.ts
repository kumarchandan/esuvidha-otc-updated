import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router
} from '@angular/router';
import {
  GlobalService
} from 'src/app/services/global/global';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  MatSnackBar
} from '@angular/material/snack-bar';


@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.scss']
})
export class CreateTicketComponent implements OnInit {
  inputForm: FormGroup;
  subject: string;
  issue: string;
  description: string;
  access_token: string;
  submitted = false;
  constructor(
    private global: GlobalService, fb: FormBuilder, private http: HttpClient, private snackBar: MatSnackBar
  ) {

    this.global.loaderOff();
    this.inputForm = new FormGroup({
      'subject': new FormControl('', [Validators.required]),
      // 'issue': new FormControl('', Validators.required),
      'priority': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.required)
    });
  }

  createTicket() {
    this.submitted = true;
    let formData = {
      "subject": this.inputForm.controls.subject.value,
      // issue: this.inputForm.controls.issue.value,
      "priority": this.inputForm.controls.priority.value,
      "issue": this.inputForm.controls.description.value,
      "group": sessionStorage.getItem("groups"),
      "users": sessionStorage.getItem("users"),
      "type": "5d134637ae3b0712fcdda633"
    }
    const header = new HttpHeaders({
      'Content-Type': 'application/json',
      'accesstoken': sessionStorage.getItem("access_token"),

    });

    console.log(formData)

    this.http.post('https://ticket.esuvidhamitra.com/api/v1/tickets/create', formData, {
      headers: header
    }).subscribe(
      (val => {
        console.log(val);
        this.snackBar.open('Ticket Generated', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      }),
      (error) => {
        console.log("Error", error);
      });
      this.inputForm.reset();
    }
    get f() {
      return this.inputForm.controls;
    }
    ngOnInit() {

    }
}