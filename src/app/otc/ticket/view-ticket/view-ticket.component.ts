import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationEnd
} from '@angular/router';
import {
  HttpHeaders,
  HttpClient
} from '@angular/common/http';
import {
  Location
} from '@angular/common'
import {
  FormGroup,
  FormBuilder,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators
} from '@angular/forms';
import {
  Subscription,
  BehaviorSubject,
  Observable,Subject
} from 'rxjs';
import {
  ErrorStateMatcher
} from '@angular/material';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from '@angular/material';
import {
  GlobalService
} from '../../../services/global/global';
import {tap}from 'rxjs/operators'
@Component({
  selector: 'app-view-ticket',
  templateUrl: './view-ticket.component.html',
  styleUrls: ['./view-ticket.component.scss']
})
export class ViewTicketComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  details: any = {};
  dtUsers: any;
  viewData = true;
  demo: any;
  inputForm: FormGroup;
x;
ate:any={};
  constructor(
    public dialog: MatDialog,
    public global: GlobalService,
    private router: Router,
    private http: HttpClient,
    public location:Location,
    private fb: FormBuilder,
    private snackBar: MatSnackBar, ) {
    this.global.loaderOff();
 

    this.inputForm = new FormGroup({
      'comment': new FormControl(),
    });




    this.details = {};
    let obj = {
      "groups": [sessionStorage.getItem("groups")],
      "users": [sessionStorage.getItem("users")]
  
    }
    console.log("=============================")
    console.log(obj);
    const header = new HttpHeaders({
      'Content-Type': 'application/json',
      'accessToken': sessionStorage.getItem("access_token")
    });

    this.http.post('https://ticket.esuvidhamitra.com/api/v1/tickets/ticket_by_user',obj,{
      headers: header
    }).subscribe((data) => {
      console.log(data);
      this.dtUsers = data["tickets"];
      this.global.loaderOff();
    }, (err) => {
      console.log(err);
      this.global.loaderOff();
      this.snackBar.open('Error! Try again.', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    });


  }
  // private _refresh= new Subject<void>();
  // get refreshAll(){
  //   return this._refresh
  // }


  ngOnInit() {
//     this.refreshAll.subscribe(()=>{
// this.addComment();
//     });
    
  
  }

  
  showDetail(user) {
    let userComment = user.comments.map((value) => {
      let obj = Object.assign({}, value);
      let d = new Date(value.date);
      obj.time = d.getHours() + ':' + d.getMinutes();
      return obj;
    })
    user.comments = userComment;
    this.details = user;
    this.viewData = false;
    console.log(this.details);
  }

  addComment(){

    let formData = {
    "comment": this.inputForm.controls.comment.value,
    "_id": this.details._id,
    "ownerId": this.details.owner._id
    }

    const header = new HttpHeaders({
      'Content-Type': 'application/json',
      'accesstoken': sessionStorage.getItem("access_token"),
    });

    console.log(formData)

  this.http.post('https://ticket.esuvidhamitra.com/api/v1/tickets/addcomment', formData, {
    headers: header
  })
  // .pipe(
  //   tap(()=>{
  //     this._refresh.next(); 
  //   })
  // )
  
  .subscribe(
    (val => {
      console.log(val);
      /* var currentLocation = window.location.toString();
      console.log("current url=",currentLocation)
      this.router.navigateByUrl(currentLocation,{skipLocationChange:true}).then(()=> {
        console.log('this is reloaded')
        this.router.navigate([decodeURI(this.location.path())]);
      }); */
      
       this.viewData = true;
      this.snackBar.open('New Comment Added', 'Dismiss', {
        duration: 8000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    }),
    error => {

      console.log("Error", error);

    })
   
    
  }

  back() {
    this.viewData = true;

  }

  toCapital(val) {
 
    var yk = val.owner.fullname.slice(0, 1);
 
    return yk.toUpperCase();
  }
}
