import {
  Component,
  OnInit,
  EventEmitter
} from '@angular/core';
import {
  MatIconRegistry,
  MatSnackBar,
  MatSlideToggleChange
} from '@angular/material';
import {
  GlobalService
} from 'src/app/services/global/global';

import {
  Router
} from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators
} from '@angular/forms';
import {
  Observable
} from 'rxjs';
import {
  ErrorStateMatcher
} from '@angular/material';
import {
  LedgerService
} from 'src/app/services/ledger.service';

@Component({
  selector: 'app-view-appointment',
  templateUrl: './view-appointment.component.html',
  styleUrls: ['./view-appointment.component.scss']
})


 
export class ViewAppointmentComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  fullappointmentDetail = [];
  sec = [];
  details;
  viewData = true;
  final
  constructor(public global: GlobalService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router: Router,
    private Ledger: LedgerService) {
    this.global.loaderOff();

  }




  showStatus(item) {
    if (item == "P") {
      return 'Pending'
    } else if (item == 'F') {
      return 'In Progress'
    } else {
      return 'Completed'
    }
  }

  ngOnInit() {


    this.global.loaderOff();
    const input = {
      center_code: sessionStorage.getItem('centre_code')
    }



    console.log(input);

    this.Ledger.getAppointments(input).then(async (data) => {
      console.log(data)
      this.fullappointmentDetail = await data.getAppointments;

      if (this.fullappointmentDetail) {
        this.fullappointmentDetail.forEach((item) => {
          var obj = {
            appointment_id: item.appointment_id,
            consumer_name: item.consumer_name,
            consumer_email: item.consumer_email,
            consumer_phone_no: item.consumer_phone_no,
            appointment_date: item.appointment_date,
            description: item.description,
            service: item.service,
            status: this.showStatus(item.status),
            region: item.region,
            start_time: item.start_time,
            end_time: item.end_time,


          }
          this.sec.push(obj);
          console.log(this.sec)
        });
      }

    });




  }
  
  onStatus(ob: MatSlideToggleChange) {
    console.log("checked")
    console.log(ob.checked);
  if(ob.checked==true){
    this.final_request('300');
  }
  else{
    this.final_request('400');
  }
  }


  final_request(status) {
    console.log("^^^^^^^^^^^^^^^^^^^666")
    console.log(this.details.appointment_id);
    this.final = {
      done_by: sessionStorage.getItem('username'),
      appointment_id: this.details.appointment_id,
      status:status
    }
    this.Ledger.finishAppointment(this.final).then(async (data) => {
      console.log("final data hai ye bhai")
      console.log(data)
    });
    
  }

  showDetail(item) {
    this.viewData = false;
    this.details = item;
    console.log("##################");
    console.log(this.details);
    // if(this.details.status==)
  }






  back() {
    this.global.loaderOn();
    this.viewData = true;
    this.global.loaderOff();
   

  }

}