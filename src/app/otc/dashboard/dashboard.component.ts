// tslint:disable:radix

import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {
  Router,
  NavigationEnd
} from '@angular/router';

import {
  MatIconRegistry,
  MatSnackBar
} from '@angular/material';

import {
  GlobalService
} from '../../services/global/global';
import {
  AuthService
} from '../../services/global/auth.service';
import {
  LedgerService
} from '../../services/ledger.service';

export interface AttendanceItem {
  days: number;
  hours: number;
  min: number;
  sec: number;
}

const attendance: AttendanceItem = {
  days: 0,
  hours: 0,
  min: 0,
  sec: 0
};

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  public opened_ledgerData = null;

  constructor(public global: GlobalService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private ledgerService: LedgerService,
    private router: Router
  ) {
    // if (!sessionStorage.getItem('oledger_uid')) {
    this.authService.openLedgerDetails().then((data) => {
        if (data.status === 'E') {
          this.snackBar.open('No ledger is opened.', 'Dismiss', {
            duration: 8000,
            horizontalPosition: 'center',
            verticalPosition: 'top'
          });
        } else {
          this.opened_ledgerData = data.dat[0];
          sessionStorage.setItem('oledger_uid', JSON.stringify(data.dat[0]));
        }

        global.loaderOff();
      })
      .catch((error) => {
        global.loaderOff();
        this.snackBar.open('Could not get Opened Ledger Details. Refresh Page!', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });
    // } else {
    //   this.opened_ledgerData = JSON.parse(sessionStorage.getItem('oledger_uid'));
    // }

    // Attendance ---------------------------------------------------------------
    let total_sec = 0;
    // days
    total_sec = total_sec + parseInt(sessionStorage.getItem('attendance_days')) * 24 * 60 * 60;
    // Hrs
    total_sec = total_sec + parseInt(sessionStorage.getItem('attendance_hrs')) * 60 * 60;
    // Min
    total_sec = total_sec + parseInt(sessionStorage.getItem('attendance_min')) * 60;
    // Sec
    total_sec = total_sec + parseInt(sessionStorage.getItem('attendance_sec'));

    attendance.days = Math.floor(total_sec / (3600 * 24));
    total_sec -= attendance.days * 3600 * 24;

    attendance.hours = Math.floor(total_sec / 3600);
    total_sec -= attendance.hours * 3600;

    attendance.min = Math.floor(total_sec / 60);
    total_sec -= attendance.min * 60;

    attendance.sec = total_sec;

  }

  showAttendance() {
    return attendance;
  }

  currentTime() {
    return new Date();
  }

  ngOnInit() {}

  // --------------- create ledger ---------------------
  createNewLedger() {
    this.global.loaderOn();
    this.ledgerService.createNewLedger().then((data) => {
        this.global.loaderOff();
        this.snackBar.open('New Ledger created!', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
        this.opened_ledgerData = data.dat;
        sessionStorage.setItem('oledger_uid', JSON.stringify(this.opened_ledgerData));
      })
      .catch((error) => {
        this.global.loaderOff();
        this.snackBar.open('New ledger not created. Retry.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });
  }

  // --------------- new transaction ---------------------
  createNewTransaction() {
    this.global.loaderOn();
    this.router.navigate(['/mods/newtransaction']);
    // this.ledgerService.createNewTransaction().then((data) => {
    //   this.global.loaderOff();
    // })
    // .catch((error) => {
    //   this.global.loaderOff();
    //   this.snackBar.open('Error. Retry.', 'Dismiss', {
    //     duration: 8000,
    //     horizontalPosition: 'center',
    //     verticalPosition: 'top'
    //   });
    // });
  }

  // --------------- close ledger ---------------------
  closeLedger() {
    this.global.loaderOn();
    this.ledgerService.closeLedger(this.opened_ledgerData.ledger_uid).then((data) => {
        this.global.loaderOff();
        this.opened_ledgerData.led_status_name = 'CLOSED';
        const obj: any = {
          led_status_name: 'CLOSED',
          ledger_uid: this.opened_ledgerData.ledger_uid
        };
        sessionStorage.setItem('oledger_uid', JSON.stringify(obj));
        this.snackBar.open('Ledger Closes with UID ' + data.dat + '!', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      })
      .catch((error) => {
        this.global.loaderOff();
        this.snackBar.open('Error. Retry.', 'Dismiss', {
          duration: 8000,
          horizontalPosition: 'center',
          verticalPosition: 'top'
        });
      });
  }

  // --------------- freeze ledger ---------------------
  freezeLedger() {
    this.global.loaderOn();
    this.router.navigate(['/mods/settle-denomination']);
  }

}
