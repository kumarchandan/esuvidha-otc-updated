import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { OtcComponent } from './otc/otc.component';

import { ViewAppointmentComponent } from './otc/appointment/view-appointment/view-appointment.component';

 const routes: Routes = [
//{ path: 'viewAppointment', component: ViewAppointmentComponent },
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
