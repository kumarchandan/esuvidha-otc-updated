import {
  Router
} from '@angular/router';
import {
  Injectable,
  OnInit
} from '@angular/core';

import {
  HttpClient
} from '@angular/common/http';

import {
  AuthService
} from './global/auth.service';

// Apollo
import gql from 'graphql-tag';
import {
  Apollo
} from 'apollo-angular';
import {
  Subscription,
  BehaviorSubject,
  Observable,Subject
} from 'rxjs';

import {
  HttpLink
} from 'apollo-angular-link-http';
import {
  InMemoryCache
} from 'apollo-cache-inmemory';

import {
  MatSnackBar
} from '@angular/material';
import {
  promise
} from 'protractor';

// tslint:disable:max-line-length
// Queries and Mutations
// tslint:disable:prefer-const
let postPaymentData;
//let bankNameDetails;

// Fetch all ledger of given agent
const allLedgersQuery = gql `
      query viewAllLedgerOnStatus($status: String!){
        viewAllLedgerOnStatus(input: {status: $status}) {
              ledger_uid
              inserted_date
              inserted_by
              total_tnx
              received_amt_cash
              received_amt_card
              received_amt_chq
              status
              total_cash_cnt
              total_card_cnt
              total_cheq_cnt
              total_dispute_cnt
              total_dispute_amt
            }
}`;

// To Fetch All Transactions of ledger
const ledgerDetailsQuery = gql `
            query viewLedgerDtls($ledger_uid: String!) {
                  viewLedgerDtls(ledger_id: $ledger_uid) {
                        tnx_uid
                        ledger_uid
                        inserted_date
                        inserted_by
                        tnx_status_name
                        con_acct_id
                        con_contact_num
                        payment_mode
                        bill_amt
                        paid_amt
                        service_name
                        remark
                      }
                    }
  `;

// Create New Ledger
const createNewLedgerMut = gql `
        mutation createNewLedger($counter_no: String!) {
                  createNewLedger(counter_no: $counter_no) {
                    counter_no
                    centre_code
                    inserted_by
                    message
                    ledger_uid
                    led_status_uid
                    led_status_name
                  }
        }
  `;

// Create New Transaction
const createNewTransaction = gql `
  mutation createNewTransaction($counter_no: String!) {
          createNewTransaction(counter_no: $counter_no) {
            counter_no
            centre_code
            status_name
            inserted_by
            role_id
            tnx_uid
            message
          }
  }
  `;
//center select
const center_select = gql `
  query  center_select{
    center_select{
              div_code
              div_name
      }
  
  }`
// Fetch Appoint Details
//CollectionSummaryReport
const appointmentList = gql `
query appointmentList($date: String, $center_select: String){
  appointmentList(input:{date:$date,center_select:$center_select})
    query_data{
        apt_no
        name
        email
        phone_no
        slot
      description
       
    }
}
`

// Close Ledger
const closeLedgerMut = gql `
  mutation updateNewLedgerOnClose($ledger_uid:String!) {
    updateNewLedgerOnClose(input:{ledger_uid: $ledger_uid}) {
      card {
        pay_uid
        tnx_uid
        tnx_status_uid
        paid_amount
        card_type
        card_tnx_uid
        pos_num
      },
      cheque {
        pay_uid
        tnx_uid
        tnx_status_uid
        paid_amount
        cheque_num
        bank_name
        cheq_due_date
        chq_dd_status
        mode_type
        rtgs_num
        neft_num
      },
      sumAmt {
        total_sum
        card_sum
        cash_sum
        cheque_sum
        dispute_sum
      }
    }
  }
  `;

// Freeze Ledger
const freezeLedgerMut = gql `
        mutation updateNewLedgerOnFreeze($ledger_uid:String!, $rs_2000: Int, $rs_500: Int, $rs_200: Int, $rs_100: Int, $rs_50: Int, $rs_20: Int, $rs_10: Int, $rs_5: Int, $rs_2: Int, $rs_1: Int, $total_sum: Float, $total_cash_amt: Float, $total_card_amt: Float, $total_cheq_amt: Float) {
          updateNewLedgerOnFreeze(input:{ledger_uid: $ledger_uid, rs_2000: $rs_2000, rs_500: $rs_500,rs_200: $rs_200, rs_100: $rs_100, rs_50: $rs_50, rs_20: $rs_20, rs_10: $rs_10, rs_5: $rs_5, rs_2: $rs_2, rs_1: $rs_1, total_sum: $total_sum, total_cash_amt: $total_cash_amt, total_card_amt : $total_card_amt, total_cheq_amt: $total_cheq_amt}) {
            card {
              pay_uid
              tnx_uid
              tnx_status_uid
              paid_amount
              card_type
              card_tnx_uid
              pos_num
            },
            cheque {
              pay_uid
              tnx_uid
              tnx_status_uid
              paid_amount
              cheque_num
              bank_name
              cheq_due_date
              chq_dd_status
              mode_type
              rtgs_num
              neft_num
            }
          }
        }
  `;

// FetchBill Rural(Fluent Grid Service)
// const fetchBillRural = gql`
//       query viewBillDtlRural($con_acct_id: String!) {
//         viewBillDtlRural(con_acct_id: $con_acct_id) {
//           account_no
//           bill_amt
//           consumer_name
//           bill_no
//           bill_due_date
//           payment_date
//           discom_name
//           division_name
//           con_contact_num
//           service_name
//           tnx_uid
//         }
//       }
// `;

// FetchBill Urban(HCL Service)
// const fetchBillUrban = gql`
//       query viewBillDtlUrban($con_acct_id: String!) {
//         viewBillDtlUrban(con_acct_id: $con_acct_id) {
//           account_no
//           bill_amt
//           consumer_name
//           bill_no
//           bill_due_date
//           payment_date
//           discom_name
//           division_name
//           con_contact_num
//           service_name
//           tnx_uid
//         }
//       }
// `;

// Fetch Bill Mutation
const fetchBillMut = gql `
        mutation fetchBill($tnx_uid: String!,$con_acct_id: String!,$contact_num: String!,$service_code: String!,$region: String!){
          fetchBill(input:{tnx_uid: $tnx_uid,con_acct_id: $con_acct_id,contact_num: $contact_num,service_code: $service_code,region: $region}) {
            account_no
            bill_amt
            consumer_name
            bill_no
            tnx_uid
            service_name
            bill_date
            discom_name
            division_name
            last_payment_date
            contact_no
            address
            inserted_by
            message
            provider
            region
          }
        }
  `;

// Post Transaction Mutation
const setPaymentCashMut = gql `
        mutation setPaymentCash($tnx_uid: String, $bill_amount: Float, $con_acct_id: String,
          $paid_amt: Float, $provider: String, $received_amt: Float, $service_code: String, $region: String,
          $bill_id: String, $counter_no: String) {

            setPaymentCash(input: {
              tnx_uid: $tnx_uid
              bill_amount: $bill_amount
              con_acct_id: $con_acct_id
              paid_amt: $paid_amt
              provider: $provider
              received_amt : $received_amt
              service_code: $service_code
              region: $region
              bill_id: $bill_id
              counter_no: $counter_no
            }) {
              tnx_uid
              inserted_date
              con_acct_id
              con_name
              counter_no
              con_mobile_no
              counter_id
              paid_amount_in_word
              address
              provider
              bill_amount
              paid_amount
              payment_mode
              tpsl_id
              uppcl_id
              posting_id
              message
            }
          }
  `;

// EDIT TNX

const editTransaction = gql `
    mutation editTransactionDtl($tnx_uid: String!, $edit_key: String!, $edit_old_val: String!, $edit_new_val: String!, $remark: String!, $is_con_acct_id: Boolean!, $payment_mode: String!) {
      editTransactionDtl(input: {
        tnx_uid: $tnx_uid,
        edit_key: $edit_key,
        edit_old_val: $edit_old_val,
        edit_new_val: $edit_new_val,
        remark: $remark,
        is_con_acct_id: $is_con_acct_id,
        payment_mode: $payment_mode
        }) {
          code
          message
        }
      }
  `;

// const bankName = gql `
//         query getAvailableBanks {
//           getAvailableBanks {
//               bank_uid
//               bank_name
//             }
//           }`; // Add response variable names by replacing code and message

const setPaymentCardMut = gql `
        mutation setPaymentCard($tnx_uid: String, $bill_amount: Float,$con_acct_id: String,
          $paid_amt: Float,$pos_num: String, $provider: String,$card_type: String,$service_code: String,
          $region: String,$bill_id: String,$counter_no: String) {

            setPaymentCard(input: {
              tnx_uid: $tnx_uid
              bill_amount: $bill_amount
              con_acct_id: $con_acct_id
              paid_amt: $paid_amt
              pos_num: $pos_num
              provider: $provider
              card_type: $card_type
              service_code: $service_code
              region: $region
              bill_id: $bill_id
              counter_no: $counter_no
            }) {
              tnx_uid
              inserted_date
              con_acct_id
              con_name
              counter_no
              con_mobile_no
              counter_id
              paid_amount_in_word
              address
              provider
              bill_amount
              paid_amount
              payment_mode
              bank_name
              pos_num
              card_type
              tpsl_id
              uppcl_id
              posting_id
              message
            }
          }
  `;

// Add response variable names by replacing code and message
const setPaymentChequeMut = gql `
        mutation setPaymentCheque($tnx_uid: String, $bill_amount: Float,$con_acct_id: String,
          $paid_amt: Float,$provider: String,$cheque_num: String,$rtgs_num: String, $date: String, $service_code: String,
          $region: String,$bill_id: String, $counter_no: String,$mode_type:String) {

            setPaymentCheque(input: {
              tnx_uid: $tnx_uid
              bill_amount: $bill_amount
              con_acct_id: $con_acct_id
              paid_amt: $paid_amt
              provider: $provider
              cheque_num: $cheque_num
              rtgs_num:$rtgs_num
              mode_type:$mode_type
              #bank_name: $bank_name
              date: $date
              service_code: $service_code
              region: $region
              bill_id: $bill_id
              counter_no: $counter_no
            }) {
              tnx_uid
              inserted_date
              con_acct_id
              con_name
              counter_no
              con_mobile_no
              counter_id
              paid_amount_in_word
              address
              bill_amount
              paid_amount
              payment_mode
              provider
              cheque_num
              cheq_due_date
              bank_name
              rtgs_num
              tpsl_id
              posting_id
              uppcl_id
              mode_type
              message
            }
          }
  `;

// Add response variable names by replacing code and message

// PAYMENT MODE DETAILS
const paymentModeDtls = gql `
        query paymentModeDtls ($tnx_uid: String!, $payment_mode: String!){
          paymentModeDtls (tnx_uid: $tnx_uid, payment_mode: $payment_mode){
            received_amount
            paid_amount
            return_amount
            card_type
            pos_num
            cheque_num
            bank_name
            cheq_due_date
            mode_type
            rtgs_num
            neft_num
          }
  }
  `;

// CREATE DISPUTE

const createNewDispute = gql `
    mutation createNewDispute($tnx_uid: String!, $remark: String!, $ledger_uid: String!, $dispute_amt: String!, $assigned_to: String!, $amt_return_mode: String, $pos_number: String, $cheque_number: String) {
      createNewDispute(input: {
      tnx_uid: $tnx_uid, remark: $remark, ledger_uid: $ledger_uid, dispute_amt: $dispute_amt, assigned_to: $assigned_to, amt_return_mode: $amt_return_mode, pos_number: $pos_number, cheque_number: $cheque_number
        }) {
          code
          message
        }
      }
  `;

// FETCH CLOSED LEDGER DETAILS

const fetchClosedLedgerDtls = gql `
      query fetchClosedLedgerDtls ($ledger_uid: String!, $status: String!){
        fetchClosedLedgerDtls (input: {ledger_uid: $ledger_uid, status: $status}){
          total_sum
          card_sum
          cash_sum
          cheque_sum
          dispute_sum
          total_tnx
          cardDtls {
            pos_num
            paid_amount
          },
          chequeDtls {
            cheque_num
            bank_name
            cheq_due_date
            inserted_date
            paid_amount
          }
        }
      }
  `;

// FETCH FREEZED LEDGER DENOMINATIONS

const fetchFreezedLedgerDenos = gql `
      query getLedgerDenominationDtls ($ledger_uid: String!, $status: String!){
        getLedgerDenominationDtls (input: {ledger_uid: $ledger_uid, status: $status}){
          rs_2000
          rs_500
          rs_200
          rs_100
          rs_50
          rs_20
          rs_10
          rs_5
          rs_2
          rs_1
          total_cash_amt
        }
      }
  `;

// CANCEL TNX MUTATION

const cancelTnx = gql `
    mutation cancelTransaction ($tnx_uid: String!, $remark: String){
        cancelTransaction (input: {tnx_uid: $tnx_uid, remark: $remark}){
          tnx_uid
          status
          message
        }
      }
  `;

// GET ALL TNX ON LEDGER : ANY STATUS
const viewAllLedgerTnxOnStatus = gql `
  query viewAllLedgerTnxOnStatus ($ledger_uid: String!, $status: String!){
    viewAllLedgerTnxOnStatus (input: {ledger_uid: $ledger_uid, status: $status}){
      tnx_uid
      ledger_uid
      inserted_date
      inserted_by
      paid_amount_in_word
      tnx_status_name
      con_acct_id
      con_contact_num
      payment_mode
      bill_amt
      paid_amt
      service_name
      remark
    }
  }`;

// GET ALL TNX DTLS ON GIVEN TNX_UID
const viewAllTnxDtls = gql `
  query viewBillDetails ($tnx_uid: String!){
    viewBillDetails (input: {tnx_uid: $tnx_uid}){
      ledger_uid
      tnx_uid
      tnx_status_name
      centre_code
      counter_no
      centre_name
      centre_address
      centre_city
      inserted_date
      user_type
      con_acct_id
      con_contact_num
      service_name
      payment_mode
      bill_amt
      paid_amt
      fname
      mname
      lname
      email
      consumer_name
      bill_no
      bill_month
      bill_due_date
      payment_date
      discom_name
      division_name
      address
      mobile_number
      email_id
      last_paid_amount
      rpdrp_nonrpdrp
      cheque_num
      bank_name
      cheq_due_date
      mode_type
      rtgs_num
      neft_num
      card_type
      pos_num
      tpsl_id
      uppcl_id
      posting_id
    }
  }`;

//Set apoointment on appointment table
const getAppointments = gql `
  query getAppointments($center_code: String!){
    getAppointments(input: {center_code: $center_code}){
      appointment_id
      consumer_name
      consumer_email
      consumer_phone_no
      appointment_date
      description
      service
      status
      region
      start_time
      end_time
    }
  }`;

//Finished Appointment
const finishAppointment = gql `
mutation finishAppointment(
  $done_by: String,
  $appointment_id: String,
  $status:String)
  {
    finishAppointment(input:{done_by:$done_by
      appointment_id:$appointment_id
    status:$status}
      ){
        appointment_id
        done_at
        msg
        status
      }
  }`;

  //Appointment Count
const getAppointmentCount=gql `
query getAppointmentCount($center_code: String)
  {
    getAppointmentCount(input:{center_code:$center_code}){
      appointment_date
      status
      count
      center_code
    }
  }
`;
export interface LedgerElement {
  ledger_uid: string;
  created_on: string;
  total_transaction: number;
  cash: number;
  cheque: number;
  card: number;
  status: string;
}

export interface NewTransactionDetails {
  counter_no: string;
  centre_code: string;
  status_name: string;
  inserted_by: string;
  role_id: string;
  tnx_uid: string;
  message: string;
}

export interface CloseLedgerDetails {
  card: {
    pay_uid: String;
    tnx_uid: String;
    tnx_status_uid: String;
    paid_amount: String;
    card_type: String;
    card_tnx_uid: String;
    pos_num: String;
  };

  cheque: {
    pay_uid: String
    tnx_uid: String
    tnx_status_uid: String
    paid_amount: String
    cheque_num: String
    bank_name: String
    cheq_due_date: String
    chq_dd_status: String
    mode_type: String
    rtgs_num: String
    neft_num: String
  };

  sumAmt: {
    total_sum: String
    card_sum: String
    cash_sum: String
    dispute_sum: String
    cheque_sum: String
  };
}

export interface FetchedBillDetails {
  account_no: string;
  bill_amt: string;
  consumer_name: string;
  bill_no: string;
  tnx_uid: string;
  service_name: string;
  bill_date: string;
  discom_name: string;
  division_name: string;
  last_payment_date: string;
  contact_no: string;
  address: string;
  provider: string;
  region: string;
  inserted_by: string;
  message: string;
}

// All the ledgers details
let ledgers: LedgerElement[] = [];
let currentTransactions = [];
let currentOperatingLedger: string;
let paymentModeDtls_data;

let closedLD_fetch;
let freezedLD_fetch;

export interface ReceiptData {
  amt_return_mode: ' ';
  centre_code: ' ';
  tnx_uid: ' ';
  dispute_amt: ' ';
  ledger_uid: ' ';
  receipt_no: ' ';
}

export interface ClosedLedgerData {
  total_sum: String;
  card_sum: String;
  cash_sum: String;
  dispute_sum: String;
  cheque_sum: String;
}

let _receiptData: ReceiptData[];

let showSidenav = true;

// For new transaction Bill details
// tslint:disable:variable-name
let newtransaction_details: NewTransactionDetails[] = [];
let fetchedbill_details: FetchedBillDetails[] = [];
let closeLedger_details: CloseLedgerDetails[] = [];

@Injectable()
export class LedgerService implements OnInit {
  private querySubscription: Subscription;
  private sharedValues: BehaviorSubject < ReceiptData[] > = new BehaviorSubject([]);
  sharedData: Observable < ReceiptData[] > = this.sharedValues.asObservable();

  private closedLedgerResponse: BehaviorSubject < ClosedLedgerData[] > = new BehaviorSubject([]);
  closedLedgerData: Observable < ClosedLedgerData[] > = this.closedLedgerResponse.asObservable();


  constructor(
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private apollo: Apollo,
    private http: HttpClient,
    private router: Router,
    httpLink: HttpLink) {
    // Temp
    // this.callLedgerQuery();
    // this.bankNames();
  }

  ngOnInit() {}
  // Sidenav -------------------------------
  toggleSidenav() {
    if (showSidenav === true) {
      showSidenav = false;
      return showSidenav;
    } else {
      showSidenav = true;
      return showSidenav;
    }
  }

  sidenavStatus() {
    return showSidenav;
  }
  // ---------------------------------------
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top',
      horizontalPosition: 'center'
    });
  }

  getPostPaymentData() {
    return postPaymentData;
  }

  // Query Microservice for all the ledgers details
  callLedgerQuery() {
    return new Promise((resolve, reject) => {
      this.apollo
        .query < any > ({
          query: allLedgersQuery,
          variables: {
            status: 'FREEZED'
          }
        })
        .subscribe(({
          data
        }) => {
          data = data.viewAllLedgerOnStatus;
          resolve(data);
        }, (error) => {
          console.log('Ledger query not working');
          console.log('Error:', error);
          reject(error);
        });
    });
  }

  createNewLedger(): any {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: createNewLedgerMut,
          variables: {
            counter_no: sessionStorage.getItem('counter_no')
          }
        })
        .subscribe(({
          data
        }) => {
          resolve({
            status: 'F',
            dat: data.createNewLedger
          });
        }, (error) => {
          console.log('Error occured in creating new ledger query:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  // Close active ledger
  closeLedger(ledger): any {
    return new Promise((resolve, reject) => {
      closeLedger_details = [];
      console.log(ledger);
      this.apollo.mutate({
          mutation: closeLedgerMut,
          variables: {
            ledger_uid: ledger
          }
        })
        .subscribe(({
          data
        }) => {
          resolve({
            status: 'F',
            dat: ledger
          });
        }, (error) => {
          reject({
            status: 'E'
          });
          console.log('Error occured in closing ledger query:', error);
        });
    });
  }

  // Fetch Closed Ledger Details of given ledger
  fetchClosedLedgerQuery(ledger_uid, status): any {

    console.log('Ledger UID:', ledger_uid);
    console.log('Ledger status:', status);
    let input = {
      ledger_uid: ledger_uid.toString(),
      status: status
    };

    return new Promise((resolve, reject) => {
      this.apollo
        .query < any > ({
          query: fetchClosedLedgerDtls,
          variables: input
        })
        .subscribe(({
          data
        }) => {
          console.log('Data', data);

          if (data.fetchClosedLedgerDtls.length < 1) {
            resolve({
              status: 'E'
            });
          } else {
            resolve({
              status: 'F',
              dat: data.fetchClosedLedgerDtls
            });
          }
        }, (error) => {
          console.log('Closed Ledger Details get query not working');
          console.log('Error:', error);

          reject({
            status: 'E'
          });
        });
    });
  }

  setFetchedClosedLD(): any {
    return closedLD_fetch;
  }

  // Fetch Closed Ledger Details of given ledger
  fetchFreezedLedgerDenos(ledger_uid, status): any {

    console.log('Ledger UID:', ledger_uid);
    console.log('Ledger status:', status);
    let input = {
      ledger_uid: ledger_uid.toString(),
      status: status
    };

    return new Promise((resolve, reject) => {

      this.apollo
        .query < any > ({
          query: fetchFreezedLedgerDenos,
          variables: input
        })
        .subscribe(({
          data
        }) => {
          console.log('Data', data);

          if (data.getLedgerDenominationDtls.length < 1) {
            resolve({
              status: 'E'
            });
          } else {
            resolve({
              status: 'F',
              dat: data.getLedgerDenominationDtls
            });
          }
        }, (error) => {
          console.log('Closed Ledger Details get query not working');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }
//appointment count
getAppointmentCount(input): any {
  return new Promise((resolve, reject) => {
    this.apollo.query({
        query: getAppointmentCount,
        variables: {
          center_code: input.center_code,
          }
      })
      .subscribe(({
        data
      }) => {
        resolve(data);
      }, (error) => {
        reject(error)
      });
  });
}

  //View Appointment list
  getAppointments(input): any {
    return new Promise((resolve, reject) => {
      this.apollo.query({
          query: getAppointments,
          variables: {
            center_code: input.center_code,
          }
        })
        .subscribe(({
          data
        }) => {
          resolve(data);
        }, (error) => {
          reject(error)
        });
    });
  }

  finishAppointment(input): any {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({
        mutation: finishAppointment,
          variables: {
            done_by: input.done_by,
            appointment_id: input.appointment_id,
            status:input.status
          }
        })
        .subscribe(({
          data
        }) => {
          resolve(data);
        }, (error) => {
          reject(error)
        });
    });
  }


  setFetchedFreezedLD(): any {
    return freezedLD_fetch;
  }


  freezeLedger(rs_2000, rs_500, rs_200, rs_100, rs_50, rs_20, rs_10, rs_5, rs_2, rs_1, closedLD, closed_LD_id): any {

    return new Promise((resolve, reject) => {

      this.apollo.mutate({
          mutation: freezeLedgerMut,
          variables: {
            ledger_uid: closed_LD_id,
            inserted_by: sessionStorage.getItem('agent_uid'),
            rs_2000: Number(rs_2000),
            rs_500: Number(rs_500),
            rs_200: Number(rs_200),
            rs_100: Number(rs_100),
            rs_50: Number(rs_50),
            rs_20: Number(rs_20),
            rs_10: Number(rs_10),
            rs_5: Number(rs_5),
            rs_2: Number(rs_2),
            rs_1: Number(rs_1),
            total_sum: Number(closedLD.total_sum),
            total_cash_amt: Number(closedLD.cash_sum),
            total_card_amt: Number(closedLD.card_sum),
            total_cheq_amt: Number(closedLD.cheque_sum)
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('freeze Ledger Query:', data.updateNewLedgerOnFreeze);

          resolve({
            status: 'F',
            dat: data.updateNewLedgerOnFreeze
          });
        }, (error) => {
          console.log('Error occured in freeze ledger query:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  setClosedLedgerData() {
    console.log(closeLedger_details);
    let setBehaviorDta: ClosedLedgerData[] = [];
    let obj = {
      total_sum: closeLedger_details[0].sumAmt.total_sum ? closeLedger_details[0].sumAmt.total_sum : '0',
      card_sum: closeLedger_details[0].sumAmt.card_sum ? closeLedger_details[0].sumAmt.card_sum : '0',
      cash_sum: closeLedger_details[0].sumAmt.cash_sum ? closeLedger_details[0].sumAmt.cash_sum : '0',
      cheque_sum: closeLedger_details[0].sumAmt.cheque_sum ? closeLedger_details[0].sumAmt.cheque_sum : '0',
      dispute_sum: closeLedger_details[0].sumAmt.dispute_sum ? closeLedger_details[0].sumAmt.dispute_sum : '0'
    };
    console.log(obj);
    setBehaviorDta.push(obj);
    this.closedLedgerResponse.next(setBehaviorDta);

  }

  getClosedLedgerData(): Observable < ClosedLedgerData[] > {
    return this.closedLedgerData;
  }

  getLedgers() {
    return ledgers;
  }

  getCurrentLedger() {
    return currentOperatingLedger;
  }

  getTransactions() {
    return currentTransactions;
  }

  // Fetch Transaction Details of given ledger on status: FREEZED
  callLedgerDetailsQuery(ledgeruid): any {

    console.log('Ledger UID:', ledgeruid);

    return new Promise((resolve, reject) => {
      this.apollo
        .query < any > ({
          query: ledgerDetailsQuery,
          variables: {
            ledger_uid: ledgeruid
          }
        })
        .subscribe(({
          data
        }) => {
          let arr = data.viewLedgerDtls;
          console.log('Transaction Data in service', arr);

          arr.forEach(element => {
            // Date
            element.inserted_date = new Date(Number(element.inserted_date));
          });

          if (arr.length < 1) {
            resolve({
              status: 'E'
            });
          } else {
            resolve({
              status: 'F',
              dat: arr
            });
          }
        }, (error) => {
          console.log('Transactions get query not working');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  // Fetch Transaction Details of given ledger on any status
  fetchledgerTnxOnAnyStatus(ledgeruid, status): any {

    console.log('Ledger UID:', ledgeruid);

    return new Promise((resolve, reject) => {
      this.apollo
        .query < any > ({
          query: viewAllLedgerTnxOnStatus,
          variables: {
            ledger_uid: ledgeruid,
            status: status
          }
        })
        .subscribe(({
          data
        }) => {
          let arr = data.viewAllLedgerTnxOnStatus;
          console.log('Transaction Data in service', arr);

          arr.forEach(element => {
            // Date
            element.inserted_date = new Date(Number(element.inserted_date));
          });

          if (arr.length < 1) {
            resolve({
              status: 'E'
            });
          } else {
            resolve({
              status: 'F',
              dat: arr
            });
          }
        }, (error) => {
          console.log('Transactions get query not working');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  // Fetch Transaction Details of given tnx_uid
  fetchcompleteTnxDtls(tnx): any {

    console.log('tnx UID:', tnx);

    return new Promise((resolve, reject) => {
      this.apollo
        .query < any > ({
          query: viewAllTnxDtls,
          variables: {
            tnx_uid: tnx.toString()
          }
        })
        .subscribe(({
          data
        }) => {
          let arr = data.viewBillDetails;
          console.log('fetchcompleteTnxDtls Data in service', data);

          if (data.viewBillDetails !== null) {
            arr.forEach(element => {
              // Date
              element.inserted_date = new Date(Number(element.inserted_date));
              element.bill_due_date = new Date(Number(element.bill_due_date));
              element.payment_date = new Date(Number(element.payment_date));
            });

            if (arr.length < 1) {
              resolve({
                status: 'E'
              });
            } else {
              resolve({
                status: 'F',
                dat: arr[0]
              });
            }
          } else {
            reject({
              status: 'E'
            });
          }

        }, (error) => {
          console.log('fetchcompleteTnxDtls get query not working');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  createNewTransaction(): any {

    console.log(sessionStorage.getItem('counter_no'));

    return new Promise((resolve, reject) => {

      this.apollo.mutate({
          mutation: createNewTransaction,
          variables: {
            counter_no: sessionStorage.getItem('counter_no')
          }
        })
        .subscribe(({
          data
        }) => {
          resolve({
            status: 'F',
            dat: data.createNewTransaction
          });
        }, (error) => {
          console.log('Error in creating new transaction -- : ', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  returnNewTnxID(): any {
    console.log('---- in return new Tnx------ ', newtransaction_details);
    return newtransaction_details;
  }

  fetchBill(formValues): any {
    console.log(formValues, ' Form Values of fetchBill');

    let bill_data = {
      message: ''
    };
    return new Promise((resolve, reject) => {

      this.apollo.mutate({
          mutation: fetchBillMut,
          variables: {
            tnx_uid: formValues.tnx_uid,
            con_acct_id: formValues.con_acct_id,
            contact_num: formValues.contact_num.toString(),
            service_code: formValues.service_code,
            region: formValues.region
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('After fetchBill Mutation:', data);
          bill_data = data.fetchBill;
          resolve({
            status: 'F',
            dat: bill_data
          });
        }, (error) => {
          console.log('Error occured in fetchBill Mutation');
          console.log('Error:', error);

          this.apollo.mutate({
              mutation: cancelTnx,
              variables: {
                tnx_uid: formValues.tnx_uid,
                remark: bill_data.message
              }
            })
            .subscribe(({
              data
            }) => {
              console.log('--- cancel tnx ---- ', data);
              // reject({
              //   status: 'F',
              //   dat: bill_data.message
              // });

            }, (errors) => {
              console.log('Error occured in cancelling tnx mutation:', errors);
              // reject({
              //   status: 'F',
              //   dat: bill_data.message
              // });
            });

          reject({
            status: 'E',
            dat: error.message
          });
        });
    });
  }

  getFetchedBillDetails() {
    return fetchedbill_details;
  }

  // bankNameDetails(): any {
  //   return new Promise((resolve, reject) => {

  //     this.querySubscription = this.apollo
  //       .watchQuery < any > ({
  //         query: bankName
  //       })
  //       .valueChanges.subscribe(({
  //         data
  //       }) => {
  //         bankNameDetails = data.getAvailableBanks;
  //         // tslint:disable-next-line:prefer-for-of

  //         for (let i = 0; i < bankNameDetails.length; i++) {
  //           bankNameDetails[i].value = bankNameDetails[i].bank_uid;
  //           bankNameDetails[i].label = bankNameDetails[i].bank_name;
  //         }

  //         resolve({
  //           status: 'F',
  //           dat: bankNameDetails
  //         });
  //       }, (error) => {
  //         console.log('Bank Name get query not working');
  //         console.log('Error:', error);
  //         reject({
  //           status: 'E'
  //         });
  //       });
  //   });
  // }

  // Cancel active tnx
  cancelTnx(datas): any {
    return new Promise((resolve, reject) => {

      this.apollo.mutate({
          mutation: cancelTnx,
          variables: {
            tnx_uid: datas.tnx_uid,
            remark: datas.remark
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('Cancel tnx:', data);

          resolve({
            status: 'F',
            dat: data.cancelTnx
          });

        }, (error) => {
          console.log('Error occured in cancelling tnx mutation:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  setPaymentCash(paidAmount, receivedAmount): any {

    let bill_details = JSON.parse(sessionStorage.getItem('fetched_bill'));

    return new Promise((resolve, reject) => {

      this.apollo.mutate({
          mutation: setPaymentCashMut,
          variables: {
            tnx_uid: bill_details.tnx_uid.toString(),
            bill_amount: parseFloat(bill_details.bill_amt),
            con_acct_id: bill_details.account_no.toString(),
            paid_amt: parseFloat(paidAmount),
            provider: bill_details.provider.toString(), // Set provider
            received_amt: parseFloat(receivedAmount),
            service_code: bill_details.service_name.toString(), // Set Service Code
            region: bill_details.region.toString(), // Set region
            bill_id: bill_details.bill_no.toString(),
            counter_no: sessionStorage.getItem('counter_no').toString() // this should be string
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('After setPaymentCash Mutation:', data);

          data.setPaymentCash.inserted_date = new Date(Number(data.setPaymentCash.inserted_date));
          resolve({
            status: 'F',
            dat: data.setPaymentCash
          });
        }, (error) => {
          console.log('Error occured in setPaymentCash Mutation');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  setPaymentCard(paidAmount, posNumber, cardType): any {
    let bill_details = JSON.parse(sessionStorage.getItem('fetched_bill'));

    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: setPaymentCardMut,
          variables: {
            // Change Variables types add toString for strings and parseFloat for float
            tnx_uid: bill_details.tnx_uid.toString(),
            bill_amount: parseFloat(bill_details.bill_amt),
            con_acct_id: bill_details.account_no.toString(),
            paid_amt: parseFloat(paidAmount),
            pos_num: posNumber.toString(),
            provider: bill_details.provider.toString(), // Set provider
            card_type: cardType.toString(),
            service_code: bill_details.service_name.toString(), // Set Service Code
            region: bill_details.region.toString(), // Set region
            bill_id: bill_details.bill_no.toString(),
            counter_no: sessionStorage.getItem('counter_no').toString()
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('After setPaymentCard Mutation:', data);
          data.setPaymentCard.inserted_date = new Date(Number(data.setPaymentCard.inserted_date));
          resolve({
            status: 'F',
            dat: data.setPaymentCard
          });
        }, (error) => {
          console.log('Error occured in setPaymentCard Mutation');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  setPaymentCheque(paidAmount, chequeNumber, micrNumber, cheque_date, paperType): any {

    let bill_details = JSON.parse(sessionStorage.getItem('fetched_bill'));
    //console.log('----------------', bankNameData);
    console.log('===========', cheque_date);
    console.log('***********', paperType);
    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: setPaymentChequeMut,
          variables: {
            // Change Variables types add toString for strings and parseFloat for float
            tnx_uid: bill_details.tnx_uid.toString(),
            bill_amount: parseFloat(bill_details.bill_amt),
            con_acct_id: bill_details.account_no.toString(),
            paid_amt: parseFloat(paidAmount),
            provider: bill_details.provider.toString(), // Set provider
            cheque_num: chequeNumber.toString(),
            mode_type:paperType.toString(),
            //bank_name: bankNameData.toString(),
            rtgs_num: micrNumber.toString(),
            date: cheque_date.toString(), // Set Date
            service_code: bill_details.service_name.toString(), // Set Service Code
            region: bill_details.region.toString(), // Set region
            bill_id: bill_details.bill_no.toString(),
            counter_no: sessionStorage.getItem('counter_no').toString()
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('After setPaymentCheque Mutation:', data);
          data.setPaymentCheque.inserted_date = new Date(Number(data.setPaymentCheque.inserted_date));
          resolve({
            status: 'F',
            dat: data.setPaymentCheque
          });
        }, (error) => {
          console.log('Error occured in setPaymentCheque Mutation');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  // create Dispute
  fileDispute(input): any {
    console.log(input);
    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: createNewDispute,
          variables: {
            tnx_uid: input.tnx_uid,
            remark: input.remark,
            ledger_uid: input.ledger_uid,
            dispute_amt: input.dispute_amt.toString(),
            assigned_to: input.assigned_to,
            amt_return_mode: null,
            pos_number: null,
            cheque_number: null
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('After fileDispute Mutation:', data);
          resolve({
            status: 'F',
            dat: data.createNewDispute
          });
        }, (error) => {
          console.log('Error occured in fileDispute Mutation');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  // fetch Payment Mode Details
  getPaymentModeDetails(tnx_uid, payment_mode): any {

    return new Promise((resolve, reject) => {

      this.apollo
        .query < any > ({
          query: paymentModeDtls,
          variables: {
            tnx_uid: tnx_uid,
            payment_mode: payment_mode
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('Data', data);
          resolve({
            status: 'F',
            dat: data.paymentModeDtls
          });
        }, (error) => {
          console.log('getPayementModeDetails get query not working');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  get_paymentModeDtls(): any {
    return paymentModeDtls_data;
  }

  // save Dispute Receipt
  saveDisputeReceipt(input) {
    console.log(input);
    _receiptData = input;
    console.log(_receiptData);
  }

  getDisputeReceipt(): Observable < ReceiptData[] > {
    console.log(_receiptData);
    this.sharedValues.next(_receiptData);
    return this.sharedData;
  }

  editTnx(input): any {
    console.log(input);
    let _is_con_acct_id: boolean;

    if (input.edit_key === 'CONTACT_NUMBER') {
      _is_con_acct_id = false;
    } else if (input.edit_key === 'ACCOUNT_ID') {
      _is_con_acct_id = true;
    }

    return new Promise((resolve, reject) => {

      this.apollo.mutate({
          mutation: editTransaction,
          variables: {
            tnx_uid: input.tnx_uid,
            edit_key: input.edit_key,
            edit_old_val: input.edit_old_val,
            edit_new_val: input.edit_new_val,
            remark: input.remark,
            is_con_acct_id: _is_con_acct_id,
            payment_mode: ' '
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('After editTnx Mutation:', data);
          resolve({
            status: 'F'
          });

        }, (error) => {
          console.log('Error occured in editTnx Mutation');
          console.log('Error:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  fetchEditBill(input) {
    this.apollo.mutate({
        mutation: fetchBillMut,
        variables: {
          tnx_uid: input.tnx_uid,
          con_acct_id: input.con_acct_id,
          contact_num: input.contact_num,
          service_code: input.service_code,
          region: input.region
        }
      })
      .subscribe(({
        data
      }) => {
        // console.log('After fetchBill Mutation:', data);

        fetchedbill_details = [];
        console.log('After fetchBill Mutation:', data);
        if (data.fetchBill.isError === true) {

          const cancel_data = {
            tnx_uid: newtransaction_details[0].tnx_uid,
            remark: data.fetchBill.message,
          };
          // this.cancelTnx(cancel_data);

          newtransaction_details = [];

          this.openSnackBar(data.fetchBill.message, '');

          this.router.navigate(['/ledgers']);
          this.cancelTnx(cancel_data);
        } else {
          fetchedbill_details.push(data.fetchBill);
          console.log('Fetched Bill Array:', fetchedbill_details);

          this.router.navigate(['/checkout']);
        }
      }, (error) => {
        console.log('Error occured in fetchBill Mutation');
        console.log('Error:', error);
      });
  }


}