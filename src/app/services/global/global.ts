import {EventEmitter, Injectable} from '@angular/core';
import {Event, NavigationEnd, Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class GlobalService {

  private loader = true;

  constructor(private router: Router) {
  }

  public loaderOn() {
    this.loader = true;
  }

  public loaderOff() {
    this.loader = false;
  }

  public loaderStatus() {
    return this.loader;
  }
}
