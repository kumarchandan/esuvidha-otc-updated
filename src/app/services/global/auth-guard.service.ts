import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import {
  Injectable
} from '@angular/core';
import {
  MatSnackBar
} from '@angular/material';

import {
  AuthService
} from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private authService: AuthService) {
  }

  openSnackBar (message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const authStatus = this.authService.isAuthenticated();

    if (authStatus) {
      // logged in so return true
      return true;
    } else {
      // not logged in so redirect to login page with the return url
      this.router.navigate(['/'], {
        queryParams: {
          returnUrl: state.url
        }
      });
      this.openSnackBar('Please Login in order to continue!', 'Okay');
      return false;
    }
  }
}
