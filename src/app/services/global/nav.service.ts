import {
  EventEmitter,
  Injectable
} from '@angular/core';
import {
  Event,
  NavigationEnd,
  Router
} from '@angular/router';
import {
  BehaviorSubject
} from 'rxjs';

@Injectable()
export class NavService {
  public appDrawer: any;
  public sideNavStatus = true;
  public currentUrl = new BehaviorSubject < string > (undefined);

  constructor(private router: Router) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl.next(event.urlAfterRedirects);
        if (this.sideNavStatus) {
          this.sideNavStatus = false;
        } else {
          this.sideNavStatus = true;
        }
      }
    });
  }

  public closeNav() {
    this.appDrawer.close();
  }

  public openNav() {
    this.appDrawer.open();
  }

  public toggleNav() {
    if (this.sideNavStatus) {
      this.closeNav();
      this.sideNavStatus = false;
    } else {
      this.openNav();
      this.sideNavStatus = true;
    }
  }
}
