import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import {
  Injectable
} from '@angular/core';
import {
  MatSnackBar
} from '@angular/material';

@Injectable()
export class CPGaurd implements CanActivate {

  constructor(
    private snackBar: MatSnackBar,
    private router: Router) {}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const status = sessionStorage.getItem('tnx_status');

    if (status === 'P') {
      // logged in so return true
      return true;
    } else if (status === 'C') {
      // not logged in so redirect to login page with the return url
      this.router.navigate(['/mods/dashboard'], {
        queryParams: {
          returnUrl: state.url
        }
      });
      this.openSnackBar('Please create new Transaction!', 'Okay');
      return false;
    }
  }
}
