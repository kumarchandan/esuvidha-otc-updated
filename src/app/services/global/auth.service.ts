import {
  Router
} from '@angular/router';
import {
  Injectable,
  ɵConsole
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';

// Apollo
import gql from 'graphql-tag';
import {
  Apollo
} from 'apollo-angular';

import {
  Subscription,
  BehaviorSubject,
  Observable
} from 'rxjs';

import {
  MatSnackBar
} from '@angular/material';

import {
  GlobalService
} from './global';
import {
  resolve
} from 'q';

// tslint:disable:no-shadowed-variable

/**************************************************************************/

// Forgot Password
const forgotPassword = gql `
      mutation forgotPassword($username: String!) {
        forgotPassword(username: $username) {
              msg
            }
}
`;

// Change Password
const changepassword = gql `
      mutation changePassword($newPassword: String!, $oldPassword: String!, $repeatPassword: String!) {
        changePassword(newPassword: $newPassword, oldPassword: $oldPassword, repeatPassword: $repeatPassword) {
              msg
            }
}
`;

// Login/Signin Mutation
const authMutation = gql `
     mutation authMutation($username: String!,$password: String!) {
         auth(username: $username,password: $password){
          header_token
          agent_uid
          manager_uid
          username
          role_id
          fname
          mname
          lname
          email_id
          phone_number
          address
          centre_code
          password_expired
          attendence{
            total_days,
            total_hour,
            total_min,
            total_sec
          }
         }
       }
  `;

// Agent Attendance Logout
const agentAttendanceLogout = gql `
      mutation agentAttendenceLogout($agent_uid: String!) {
        agentAttendenceLogout(agent_uid: $agent_uid) {
          msg
        }
      }
`;

// To check current open ledger status
const viewLedgerStatus = gql `
      query viewLedgerStatus {
        viewLedgerStatus{
          led_status_name
          ledger_uid
        }
      }
`;

// To Fetch Available counter
const availableCounter = gql `
      query availableCounter($centre_code: String!) {
        availableCounter(centre_code: $centre_code) {
              counter_code
              centre_code
              centre_name
              available_status
              centre_address
            }
}
`;

// To Assign Available counter
const counterAllot = gql `
      mutation counterAllot($counter_no: String!, $centre_code: String!, $agent_uid: String!) {
        counterAllot(counter_no: $counter_no, centre_code: $centre_code, agent_uid: $agent_uid) {
              counter_no
            }
}
`;

// To Release Available counter
const counterRelease = gql `
      mutation counterRelease($counter_no: String!, $centre_code: String!, $agent_uid: String!) {
        counterRelease(counter_no: $counter_no, centre_code: $centre_code, agent_uid: $agent_uid) {
              msg
            }
}
`;


// To Set User Online
const userOnline = gql `
      subscription userOnline{
          userOnline
      }
    `;

/**************************************************************************/
/**************************************************************************/

export interface CounterElement {
  counter_code: string;
  centre_code: string;
  centre_name: string;
  available_status: string;
  centre_address: string;
}

let currentUser;
let attendance;
let openLedgerStatus;
let openLedgerUID;
let counters: CounterElement[] = [];

@Injectable()

export class AuthService {

  private querySubscription: Subscription;

  private LEDGER_UID: BehaviorSubject < string > = new BehaviorSubject('');
  Ledger_ID: Observable < string > = this.LEDGER_UID.asObservable();
  private LEDGER_STATUS: BehaviorSubject < string > = new BehaviorSubject('');
  Ledger_Status: Observable < string > = this.LEDGER_STATUS.asObservable();

  token: string;

  constructor(
    private snackBar: MatSnackBar,
    private apollo: Apollo,
    private http: HttpClient,
    private router: Router
  ) {}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 8000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
  }

  // Auth
  authenticate(uname, pswd): any {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: authMutation,
          variables: {
            username: uname.toUpperCase(),
            password: pswd,
          }
        })
        .subscribe(({
          data
        }) => {

          currentUser = data.auth;
          console.log(currentUser)
          attendance = currentUser.attendence[0];
          const stat = 'Y';
          sessionStorage.setItem('login_stat', stat);
          sessionStorage.setItem('authorization', currentUser.header_token);
          sessionStorage.setItem('username', currentUser.username);
          sessionStorage.setItem('agent_uid', currentUser.agent_uid);
          sessionStorage.setItem('centre_code', currentUser.centre_code);
          sessionStorage.setItem('attendance_days', attendance.total_days);
          sessionStorage.setItem('attendance_hrs', attendance.total_hour);
          sessionStorage.setItem('attendance_min', attendance.total_min);
          sessionStorage.setItem('attendance_sec', attendance.total_sec);
          sessionStorage.setItem('FullName', currentUser.fname + ' ' + currentUser.lname);
          if (currentUser.password_expired) {
            this.router.navigate(['/changepassword']);
            resolve(data);
          }

          this.callAvailableCounterQuery().then((datas) => {
              resolve(data);
            })
            .catch((err) => {
              reject(err);
            });

        }, (error) => {

          reject(error);
        });
    });
  }

  userOnline(): any {
    return new Promise((resolve, reject) => {
      this.apollo.subscribe({
          query: userOnline
        })
        .subscribe(
          (data) => {
            console.log('SUBSCRIBED', data);
            resolve(data);
          },
          (err) => {
            console.log('ERROR', err);
            reject(err);
          }
        );
    });
  }

  forgotPassword(username): any {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: forgotPassword,
          variables: {
            username: username
          }
        })
        .subscribe(({
          data
        }) => {
          resolve(data);
        }, (error) => {
          console.log('Error occured in agent counterRelease logout mutation');
          console.log('Error:', error);
          reject(error);
        });
    });
  }

  changePassword(newpassword, repeatnewpassword, oldpassword): any {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: changepassword,
          variables: {
            newPassword: newpassword,
            repeatPassword: repeatnewpassword,
            oldPassword: oldpassword,
          }
        })
        .subscribe(({
          data
        }) => {
          resolve(data);
        }, (error) => {
          console.log('Error occured in agent counterRelease logout mutation');
          console.log('Error:', error);
          error.status = true;
          reject(error);
        });
    });
  }





  // Assign Counter
  assignCounter(input): any {
    console.log(input);
    return new Promise((resolver, reject) => {
      this.apollo.mutate({
          mutation: counterAllot,
          variables: {
            counter_no: input.counter_no,
            centre_code: input.centre_code,
            agent_uid: input.agent_uid
          }
        })
        .subscribe(({
          data
        }) => {
          if (data.counterAllot.counter_no === input.counter_no) {
            currentUser.counter_no = data.counterAllot.counter_no;
            currentUser.centre_code = input.centre_code;
            sessionStorage.setItem('counter_no', data.counterAllot.counter_no);
            sessionStorage['login_stat'] = 'S';

            this.openSnackBar('You are now signed-in.', 'Dismiss');
            // Fetch Open Ledger Status
            // this.openLedgerDetails();
            resolver(true);
          } else {
            resolve(false);
          }

        }, (error) => {
          console.log(error);
          reject(error);
        });
    });
  }


  // Attendance
  getAttendance() {
    return sessionStorage.getItem('attendance');
  }

  // Get User Details
  getUserDetails() {
    return currentUser;
  }

  // Query Microservice for all the available counter details
  callAvailableCounterQuery(): any {
    return new Promise((resolve, reject) => {
      counters = [];

      // console.log('Username:', this.authService.getUserDetails());
      const centre_code = sessionStorage.getItem('centre_code');

      this.querySubscription = this.apollo
        .watchQuery < any > ({
          query: availableCounter,
          variables: {
            centre_code: centre_code
          }
        })
        .valueChanges.subscribe(({
          data
        }) => {
          let arr = [];
          arr = data.availableCounter;

          if (arr.length < 1) {
            this.openSnackBar('All counters are currently booked.', 'Dismiss');
            reject({
              msg: 'All Counters are currently booked',
              code: 404
            });
          }
          sessionStorage.setItem('centre_name', arr[0].centre_name);
          sessionStorage.setItem('centre_address', arr[0].centre_address);
          sessionStorage.setItem('counters', JSON.stringify(arr));
          resolve(data);
        }, (error) => {
          console.log(error);
          this.router.navigate(['/signin']);
          reject(error);
        });
    });
  }

  logout_attendance(): any {

    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: agentAttendanceLogout,
          variables: {
            agent_uid: sessionStorage.getItem('agent_uid')
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('AgentAttendance Logout:', data);
          resolve(data);
        }, (error) => {
          console.log('Error occured in agent attendance logout mutation');
          console.log('Error:', error);

          reject(error);
        });
    });
  }

  logout_counterRelease(): any {
    return new Promise((resolve, reject) => {
      this.apollo.mutate({
          mutation: counterRelease,
          variables: {
            counter_no: sessionStorage.getItem('counter_no'),
            centre_code: sessionStorage.getItem('centre_code'),
            agent_uid: sessionStorage.getItem('agent_uid')
          }
        })
        .subscribe(({
          data
        }) => {
          console.log('CounterRelease Logout:', data);
          resolve(data);
        }, (error) => {
          console.log('Error occured in agent counterRelease logout mutation');
          console.log('Error:', error);
          reject(error);
        });
    });
  }

  logout(): any {

    // Attendance Logout Mut
    return new Promise((resolve, reject) => {
      this.logout_attendance().then((data) => {
          this.logout_counterRelease().then((data) => {
              resolve({
                status: 'S'
              });

            })
            .catch((error) => {
              reject({
                status: 'E'
              });

            });
        })
        .catch((error) => {
          reject({
            status: 'E'
          });
        });
    });
  }

  isAuthenticated() {
    return sessionStorage.getItem('authorization') !== null;
  }

  // Fetch Open Ledger Details
  openLedgerDetails(): any {

    return new Promise((resolve, reject) => {
      this.apollo
        .query < any > ({
          query: viewLedgerStatus
        })
        .subscribe(({
          data
        }) => {
          console.log('Open Ledger Status:', data);
          if (data.viewLedgerStatus == null || data.viewLedgerStatus === undefined || data.viewLedgerStatus.length < 1) {
            resolve({
              status: 'E'
            });
          } else {
            resolve({
              status: 'F',
              dat: data.viewLedgerStatus
            });
          }

        }, (error) => {
          console.log('Error in openLedgerStatus:', error);
          reject({
            status: 'E'
          });
        });
    });
  }

  // Send Active Ledger Details to Action Bar
  getActiveLedgerDetails() {
    // this.openLedgerDetails();

    const r = {
      status: openLedgerStatus,
      ledger_uid: openLedgerUID
    };
    return r;
  }

  setActiveLedgerClosed(ledger) {
    openLedgerStatus = 'CLOSED';
    openLedgerUID = ledger;

    this.setActiveLedgerID();
    this.setActiveLedgerStatus();
  }

  setActiveLedgerID() {
    this.LEDGER_STATUS.next('');
    this.LEDGER_UID.next(openLedgerUID);
  }

  setActiveLedgerStatus() {
    this.LEDGER_STATUS.next('');
    this.LEDGER_STATUS.next(openLedgerStatus);
  }

  getActiveLedgerID(): Observable < string > {
    // this.openLedgerDetails();
    return this.Ledger_ID;
  }

  getActiveLedgerStatus(): Observable < string > {
    // this.setActiveLedgerStatus();
    // this.openLedgerDetails();
    return this.Ledger_Status;
  }

}
