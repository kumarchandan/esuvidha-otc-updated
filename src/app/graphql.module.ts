import {
  NgModule
} from '@angular/core';
import {
  ApolloModule,
  APOLLO_OPTIONS
} from 'apollo-angular';
import {
  HttpLinkModule,
  HttpLink
} from 'apollo-angular-link-http';
import {
  InMemoryCache
} from 'apollo-cache-inmemory';

import {
  HttpClientModule,
  HttpHeaders
} from '@angular/common/http';
import {
  setContext
} from 'apollo-link-context';
import {
  ApolloLink,
  split
} from 'apollo-link';
import {
  WebSocketLink
} from 'apollo-link-ws';
import {
  getMainDefinition
} from 'apollo-utilities';

const uri = 'https://esuvidhamitra.com/api'; // <-- add the URL of the GraphQL server here

export function createApollo(httpLink: HttpLink) {

  // const ws = new WebSocketLink({
  //   uri: `wss://esuvidhamitra.com/graphql`,
  //   options: {
  //     reconnect: true,
  //   },
  // });

  const basic = setContext((op, ctx) => ({
    headers: new HttpHeaders()
      .set('Accept', 'charset=uf-8'),
  }));

  const auth = setContext((operation, ctx) => ({
    headers: ctx.headers.append('Authorization', `Bearer ${sessionStorage.getItem('authorization')}`)
  }));

  const defaultOptions = {
    watchQuery: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all',
    }
  };

  const terminatingLink = httpLink.create({
    uri
  });

//   const terminatingLink = split(
//     ({ query }) => {
//       const { kind, operation } = getMainDefinition(query);
//       return (
//         kind === 'OperationDefinition' && operation === 'subscription'
//       );
//     },
//     httpLink.create({
//       uri
//     })
// );

  const link = ApolloLink.from([basic, auth, terminatingLink]);

  return {
    link,
    cache: new InMemoryCache(),
    defaultOptions: defaultOptions,
  };
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: [{
    provide: APOLLO_OPTIONS,
    useFactory: createApollo,
    deps: [HttpLink],
  }, ],
})
export class GraphQLModule {}
